import * as admin from "firebase-admin";
export * from "./createMessage";
export * from "./checkMessage";
export * from "./deleteMessage";
export * from "./getMessage";
export * from "./getUserMessages";

admin.initializeApp();

admin.firestore().settings({
  ignoreUndefinedProperties: true,
});
