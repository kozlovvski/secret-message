import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

import {
  CheckSMessagePayload,
  FullSMessage,
} from "../../src/typings/secret-message";

export const getMessage = functions.https.onCall(
  async ({ id }: CheckSMessagePayload) => {
    if (!(typeof id === "string") || id.length === 0) {
      // Throwing an HttpsError so that the client gets the error details.
      throw new functions.https.HttpsError("invalid-argument", "id invalid");
    }
    const querySnap = await admin
      .firestore()
      .collection("secret-messages")
      .where("id", "==", id)
      .get()
      .catch((err: Error) => {
        throw new functions.https.HttpsError("unknown", err.message);
      });

    if (querySnap.docs.length === 0) {
      throw new functions.https.HttpsError(
        "unknown",
        "There is no message with such id"
      );
    }

    if (querySnap.docs.length > 1) {
      throw new functions.https.HttpsError(
        "unknown",
        "There is more than one message with such id"
      );
    }
    const docRef = querySnap.docs[0];
    const data = docRef.data() as FullSMessage;

    if (data.alreadyViewed) {
      throw new functions.https.HttpsError(
        "unknown",
        "This document was already viewed"
      );
    }

    if (!data.message) {
      throw new functions.https.HttpsError(
        "unknown",
        "Message field is invalid"
      );
    }

    const { uid, createdAt } = data;

    admin
      .firestore()
      .collection("secret-messages")
      .doc(docRef.id)
      .set({ alreadyViewed: true, uid, createdAt, id });
    return data;
  }
);
