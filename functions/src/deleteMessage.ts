import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

import { DeleteSMessagePayload } from "../../src/typings/secret-message";

export const deleteMessage = functions.https.onCall(
  async ({ id }: DeleteSMessagePayload) => {
    if (typeof id !== "string" || id.length === 0) {
      // Throwing an HttpsError so that the client gets the error details.
      throw new functions.https.HttpsError("invalid-argument", "id invalid");
    }
    const querySnap = await admin
      .firestore()
      .collection("secret-messages")
      .where("id", "==", id)
      .get()
      .catch((err: Error) => {
        throw new functions.https.HttpsError("unknown", err.message);
      });

    if (querySnap.docs.length === 0) {
      throw new functions.https.HttpsError(
        "not-found",
        "There is no message with such id"
      );
    }

    if (querySnap.docs.length > 1) {
      throw new functions.https.HttpsError(
        "unknown",
        "There is more than one message with such id"
      );
    }

    const docRef = querySnap.docs[0];

    await admin
      .firestore()
      .collection("secret-messages")
      .doc(docRef.id)
      .delete()
      .catch((err: Error) => {
        throw new functions.https.HttpsError("unknown", err.message);
      });

    return true;
  }
);
