import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

import { GenericSMessage } from "../../src/typings/secret-message";

export const getUserMessages = functions.https.onCall(
  async (payload, context) => {
    const uid = context.auth?.uid;
    if (!uid) {
      // Throwing an HttpsError so that the client gets the error details.
      throw new functions.https.HttpsError(
        "invalid-argument",
        "No UID provided, are you logged in?"
      );
    }
    const querySnap = await admin
      .firestore()
      .collection("secret-messages")
      .where("uid", "==", uid)
      .get()
      .catch((err: Error) => {
        throw new functions.https.HttpsError("unknown", err.message);
      });

    const data: GenericSMessage[] = querySnap.docs.map((doc) => {
      const docData = doc.data();

      return {
        id: docData.id,
        alreadyViewed: docData.alreadyViewed,
        createdAt: docData.createdAt.toMillis(),
        message: undefined,
      } as GenericSMessage;
    });

    return data;
  }
);
