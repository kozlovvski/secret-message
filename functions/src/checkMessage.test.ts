import { GenericSMessage } from "../../src/typings/secret-message";
import { deleteById, testEnv } from "../setupTests";
import { checkMessage } from "./checkMessage";
import { createMessage } from "./createMessage";

describe("checkMessage", () => {
  const wrapped = testEnv.wrap(checkMessage);

  it("should reject when id is undefined", async () => {
    await expect(wrapped({})).rejects.toThrow();
  });

  it("should reject when id is not a string", async () => {
    await expect(wrapped({ id: true })).rejects.toThrow();
    await expect(wrapped({ id: 3 })).rejects.toThrow();
  });

  it("should resolve when id is a string", async () => {
    const call = wrapped({ id: "test-id" });
    expect(call).resolves.toBeTruthy;
  });

  it("should return false when id does not exist in Firestore", async () => {
    const res: boolean = await wrapped({ id: "test-TEST-123" });
    expect(res).toBe(false);
  });

  it("should return true when id does exist in Firestore", async () => {
    const { id }: GenericSMessage = await testEnv.wrap(createMessage)({
      message: "return test",
    });
    const res: boolean = await wrapped({ id });
    expect(res).toBe(true);
    deleteById(id);
  });
});

testEnv.cleanup();
