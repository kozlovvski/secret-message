import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

import { CheckSMessagePayload } from "../../src/typings/secret-message";

export const checkMessage = functions.https.onCall(
  async ({ id }: CheckSMessagePayload) => {
    if (!(typeof id === "string") || id.length === 0) {
      // Throwing an HttpsError so that the client gets the error details.
      throw new functions.https.HttpsError("invalid-argument", "id invalid");
    }
    const docSnap = await admin
      .firestore()
      .collection("secret-messages")
      .where("id", "==", id)
      .get()
      .catch((err: Error) => {
        throw new functions.https.HttpsError("unknown", err.message);
      });
    if (docSnap.size > 1) {
      throw new functions.https.HttpsError(
        "unknown",
        "More than one message with this id exists"
      );
    }
    return docSnap.size == 1;
  }
);
