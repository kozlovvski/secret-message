# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.2](https://gitlab.com/kozlovvski/secret-message/compare/v0.2.1...v0.2.2) (2020-12-16)


### Features

* **firebase:** add google analytics ([51a00dd](https://gitlab.com/kozlovvski/secret-message/commit/51a00dd283413276c21e6a2f76e9f2e18c934b0f))


### Bug Fixes

* **firebase:** wrap analytics setup in isSupported() to fix jest tests warnings ([bc60fd9](https://gitlab.com/kozlovvski/secret-message/commit/bc60fd96ccadc393264d171709474d513247d834))
* **warnings:** add refs to CSSTransition components to remove findDOMNode warnings ([dbdf8f3](https://gitlab.com/kozlovvski/secret-message/commit/dbdf8f365876a251d7e728b822c1f60b2c284238))

### [0.2.1](https://gitlab.com/kozlovvski/secret-message/compare/v0.2.0...v0.2.1) (2020-12-16)

## [0.2.0](https://gitlab.com/kozlovvski/secret-message/compare/v0.1.1...v0.2.0) (2020-12-16)


### Features

* **403:** add 403 view component and use it in appropriate routes ([28e41a3](https://gitlab.com/kozlovvski/secret-message/commit/28e41a3b39194d0d7a1874a322204804720b3a5e))
* **404:** add 404 page and adjust router config ([e4c9b13](https://gitlab.com/kozlovvski/secret-message/commit/e4c9b134271aa019d3aa4fc4588acf234ce57f6b))
* **delete-message:** add deleteMessage cloud function ([b338a19](https://gitlab.com/kozlovvski/secret-message/commit/b338a19101293c4cb9f64bf00ce9b652d8590a51))
* **delete-message:** finish deleteMessageCloud function with appropriate tests ([f12e616](https://gitlab.com/kozlovvski/secret-message/commit/f12e616b9151e60fff394ad4a4d63cafab075eaf))
* **home-page:** add Home page with necessary tests and styles ([9943789](https://gitlab.com/kozlovvski/secret-message/commit/994378930f22299f83be124bd9e0cb349be1a1cb))
* **seo:** add titles, meta tags, web manifest and favicon ([4bf2075](https://gitlab.com/kozlovvski/secret-message/commit/4bf207553dd15ee12ced1e3d76dbd87f47817d80))
* **user-panel:** add Navigation component and change App layout setup to match new setup ([e31a246](https://gitlab.com/kozlovvski/secret-message/commit/e31a24608746d73ceb8391c860cea470c8f72b77))
* **user-panel:** add navigation.slice with tests ([4ea90c9](https://gitlab.com/kozlovvski/secret-message/commit/4ea90c936c26ce88b8156751b2d434f2450cb5bf))
* **user-panel:** add user messages fetching on login ([e75f0c9](https://gitlab.com/kozlovvski/secret-message/commit/e75f0c984c0f5f9853404edfb63f12dfb246639c))
* **user-panel:** add user messages fetching on new message creation success ([98045b9](https://gitlab.com/kozlovvski/secret-message/commit/98045b9d9881f29a098e8dd8d74178dc8dca95b4))
* **user-panel:** add user-messages slice with tests ([48417bd](https://gitlab.com/kozlovvski/secret-message/commit/48417bd8c6bc28b27db3297305ee2523023692be))
* **user-panel:** change logo to a link to new message page ([c4f42ea](https://gitlab.com/kozlovvski/secret-message/commit/c4f42eac9abddac8c2904bb922d05043c67400d7))
* **user-panel:** create boilerplate YourMessages page ([0d9bb4c](https://gitlab.com/kozlovvski/secret-message/commit/0d9bb4c6e221faede088aad4dcf7491e4084fbe0))
* **user-panel:** finish MessagesList component with tests ([136f5a5](https://gitlab.com/kozlovvski/secret-message/commit/136f5a5963c066d693cdfbe97938dac3213f65ca))
* **user-panel:** finish YourMessages route ([18817b5](https://gitlab.com/kozlovvski/secret-message/commit/18817b5aabd2c7db379f0dd3c6358975aa870d85))
* **user-panel:** write getUserMessages cloud function with tests ([7ead999](https://gitlab.com/kozlovvski/secret-message/commit/7ead9991f9fa2f35c2c8d026e21a89a2306d2d43))
* **user-panel:** write MessagesList component ([f1563ee](https://gitlab.com/kozlovvski/secret-message/commit/f1563eef5f1c57877e3546363fb58944ba996e0a))
* **view-message:** add boilerplate NoMessage component ([d7c9bdf](https://gitlab.com/kozlovvski/secret-message/commit/d7c9bdf8d86fb57a983561e122c8766101755557))
* **view-message:** add boilerplate view-message files ([0047596](https://gitlab.com/kozlovvski/secret-message/commit/00475960f84de27d7d4bc67255bde9603aedd34c))
* **view-message:** add checkMessage firebase cloud function with tests ([555d529](https://gitlab.com/kozlovvski/secret-message/commit/555d52976769f8547aaf44aa4bbf4f3e771ebfef))
* **view-message:** add checkMessageCloud exported firebase cloud function ([e0fd2e0](https://gitlab.com/kozlovvski/secret-message/commit/e0fd2e054786e1846926b3016694817f12de9560))
* **view-message:** add getMessage thunk with tests ([9860c7d](https://gitlab.com/kozlovvski/secret-message/commit/9860c7d743c98263367a35e3843e79e3a07cdde8))
* **view-message:** add LoadingMessage component with appropriate tests ([13c9e9b](https://gitlab.com/kozlovvski/secret-message/commit/13c9e9b85183c0e10a046e9c857c81ee28223513))
* **view-message:** add MessageWarning component with necessary tests ([2b3668e](https://gitlab.com/kozlovvski/secret-message/commit/2b3668e7a7ab2534c12338c0c1a59b8ffe0b0d26))
* **view-message:** add view-message.slice with appropriate tests ([f2a3176](https://gitlab.com/kozlovvski/secret-message/commit/f2a3176fc2be4d640893615c2eac29f1776813dc))
* **view-message:** create a ViewMessage boilerplate with tests ([e58ce7a](https://gitlab.com/kozlovvski/secret-message/commit/e58ce7a7a34af866eeeca3bfa30f31a653473496))
* **view-message:** create getMessage firebase cloud function ([c588868](https://gitlab.com/kozlovvski/secret-message/commit/c5888681cff2c44bb1ba77a1b8c361873025df52))
* **view-message:** write basic message page to pass tests ([1172006](https://gitlab.com/kozlovvski/secret-message/commit/11720063af1bfa61a9e2d8e4f9e5ac1e7f714052))
* **view-message:** write clearMessage slice and add it to the Message page cleanup ([7334d9a](https://gitlab.com/kozlovvski/secret-message/commit/7334d9a21f07c88f2ec1bf9c59774fecac2b3ee1))
* **view-message:** write getMessageCloud firebase function ([ad000a9](https://gitlab.com/kozlovvski/secret-message/commit/ad000a90cd2d28402a8ce1d1e030fefa880140ef))
* **view-message:** write NoMessage component ([0c41715](https://gitlab.com/kozlovvski/secret-message/commit/0c41715e9c794d946e6c6a5eab2458567eaae388))
* **view-message:** write ViewMessage component ([832795b](https://gitlab.com/kozlovvski/secret-message/commit/832795b63340fd05204470532dd250477e6f4b78))


### Bug Fixes

* **add-new-message:** save createMessageCloud in own module and correct necessary tests ([2097e7d](https://gitlab.com/kozlovvski/secret-message/commit/2097e7d911e22c27def379f1a59c85cc7ca0813c))
* **delete-message:** change deleteMessage cloud function return type to void ([638237e](https://gitlab.com/kozlovvski/secret-message/commit/638237e758e0973d53f6c32f0c448405707a387f))
* **delete-message:** handle two extra edge cases in deleteMessage cloud function ([ceae24c](https://gitlab.com/kozlovvski/secret-message/commit/ceae24c5b125a35899ab87ee179b10227acfedb7))
* **getusermessages:** change getUserMessages cloud function timestamp return type to number ([43c321f](https://gitlab.com/kozlovvski/secret-message/commit/43c321f921ef50c547542b6db4316ed55ad73923))
* **routes.tsx:** change layout position in routes to prevent rerendering while transition happens ([0a70a76](https://gitlab.com/kozlovvski/secret-message/commit/0a70a762f8a97fbb00854b09c43a66eede56c63e))
* **rwd:** adjust app responsiveness ([8f3795f](https://gitlab.com/kozlovvski/secret-message/commit/8f3795f561d1c9b84beb7a9e0b1fddc053e46359))
* **styles:** fix broken styles in AuthScreenTitle ([bac70eb](https://gitlab.com/kozlovvski/secret-message/commit/bac70eb1e6c30ba42cb7e8a612d3622b2a5e87db))
* **typings:** change firebase import in secret-message.d.ts ([2d67778](https://gitlab.com/kozlovvski/secret-message/commit/2d67778246b83767cee42b502117db6df0433527))
* **user-messages:** add user-messages slice to defaultState and rootReducer ([b0de16b](https://gitlab.com/kozlovvski/secret-message/commit/b0de16b6ba02938a5997c4c64849b83c92a12a8b))
* **view-message:** change checkMessage return to actual cloud function call and write new tests ([2409178](https://gitlab.com/kozlovvski/secret-message/commit/2409178e24d0b767be4fa9095a37e810224475d6))
* **view-message:** comment out failing tests until I come up with a cloud function fix ([d936352](https://gitlab.com/kozlovvski/secret-message/commit/d936352f5f422be386a3d30f0c4042c37be366c3))
* **view-message:** edit getMessage cloud function to delete message after successful call ([471e712](https://gitlab.com/kozlovvski/secret-message/commit/471e712d214c8d793f23f6cca754221ce0ccf8cf))
* **view-message:** fix getMessageCloud to correctly update firestore value on call ([7b15587](https://gitlab.com/kozlovvski/secret-message/commit/7b15587d5a19f1734cdd08ffffa630f96e6a4cd0))
* **view-message:** update Message page with new tests and code ([e0cd52e](https://gitlab.com/kozlovvski/secret-message/commit/e0cd52e1805bdc16e5272a59c3ecf17704c90b55))

### [0.1.1](https://gitlab.com/kozlovvski/secret-message/compare/v0.1.0...v0.1.1) (2020-12-07)


### Features

* **auth:** create basic components for user authentication ([19001a7](https://gitlab.com/kozlovvski/secret-message/commit/19001a7e6ffbeb73c04fac38805d09b2aa420ea9))
* add SignInButton ([6c1cdad](https://gitlab.com/kozlovvski/secret-message/commit/6c1cdad8ca5ba561459327ff0ce9d351346389b1))
* **app layout:** extend app layout with basic tests and components ([328a37b](https://gitlab.com/kozlovvski/secret-message/commit/328a37b6518a73faa358bd120b9b77abc0153f22))
* **auth:** setup basic auth logic and redux slice ([ac3a516](https://gitlab.com/kozlovvski/secret-message/commit/ac3a5162e9d5a9cd81261620a673aa2598357abc))
* create `add message` route ([a375f25](https://gitlab.com/kozlovvski/secret-message/commit/a375f253e5b3b28a3bd852d379d15991b6558e16))
* **create-message page:** add components transition ([4cb0078](https://gitlab.com/kozlovvski/secret-message/commit/4cb007890d211da3d6b2da357439a268cf53f994))
* **create-message-comp:** add boilerplate CreateMessage component and write tests ([4e72468](https://gitlab.com/kozlovvski/secret-message/commit/4e724680596484208ee074448f6d9b65eb685f21))
* **create-message-confirm:** add boilerplate CreateMessageConfirm component ([1c6a09d](https://gitlab.com/kozlovvski/secret-message/commit/1c6a09d65baac822093b4004ee4f405f19b5b26a))
* **create-message-confirm:** write CreateMessageConfirm component ([e5101f2](https://gitlab.com/kozlovvski/secret-message/commit/e5101f2acd9b39571639876505bb3b8184b40df2))
* **create-message-page:** add exit transition to create-message-form ([1af6570](https://gitlab.com/kozlovvski/secret-message/commit/1af65703738f20f3faba0816f95d7e5ff01884c2))
* **firebase:** setup firebase in the app ([a931e7e](https://gitlab.com/kozlovvski/secret-message/commit/a931e7eae9fd5f4ce2eafa9a3dd86c16dae347fe))
* **functions:** add createMessage cloud function with tests ([24cee36](https://gitlab.com/kozlovvski/secret-message/commit/24cee365f0b8292fc5ac0dcf5838864346300367))
* **helpers:** add react-transition-group scss module classes creator ([4bf4f02](https://gitlab.com/kozlovvski/secret-message/commit/4bf4f029c849ef44cf1075b9d1a37ffcbd9db840))
* **hooks:** add a useAppDispatch typed hook with test file ([d8951ff](https://gitlab.com/kozlovvski/secret-message/commit/d8951fffc801c3fbfc0d4db25af0242de860bb89))
* **hooks:** add a useAppSelector hook with tests ([49859fc](https://gitlab.com/kozlovvski/secret-message/commit/49859fc48741f1e59f25c1e64f368f7b5420887c))
* **layout:** add basic app layout ([c4b07a2](https://gitlab.com/kozlovvski/secret-message/commit/c4b07a2c043968a1e2bffb5142630a59ecf8825b))
* **pages:** create a basic CreateMessage route ([5aae53d](https://gitlab.com/kozlovvski/secret-message/commit/5aae53d8482de71b759eeedd9e1270c07fa4bfb2))
* **scss:** improve layout design ([bce60cf](https://gitlab.com/kozlovvski/secret-message/commit/bce60cff10529b71a50aff2b266f090af41ade81))
* **typings:** add response and request payload types for firebase functions ([84a9b03](https://gitlab.com/kozlovvski/secret-message/commit/84a9b033070973f3c076e74cb8fdd9ab34e05a16))


### Bug Fixes

* **app-layout:** remove unnecessary padding ([ed9f9a7](https://gitlab.com/kozlovvski/secret-message/commit/ed9f9a74e0b1d55a6e6aa35acbf5b52b5f119bbb))
* **auth:** add correct links in new-message related components ([d2032be](https://gitlab.com/kozlovvski/secret-message/commit/d2032be8aa6f1290e4bf20866e00d41802f4ba52))
* **craete-message-confirm:** persist message link in CreateMessageConfirm after deleted from redux ([742c402](https://gitlab.com/kozlovvski/secret-message/commit/742c402db698b7b37e113701dd8ca706bda0b62c))
* **create-message page:** assign an initial value to useState value ([ed6d329](https://gitlab.com/kozlovvski/secret-message/commit/ed6d329dbb1fea99572ce3ecddd9b5c3fa7b9667))
* **create-message page:** dispatch clearMessage action in useEffect cleanup ([070e995](https://gitlab.com/kozlovvski/secret-message/commit/070e9957decbebccf693aaa10d86227ac89b94ec))
* **create-message-confirm:** finish CreateMessageConfirm component ([e6fffd0](https://gitlab.com/kozlovvski/secret-message/commit/e6fffd0f94ea27169883ed6e40b44a9f0a9b5d1a))
* **create-message-form:** add an icon to submit button ([456ccca](https://gitlab.com/kozlovvski/secret-message/commit/456ccca8eca488d9df68ceeedf2ad285c25d6b27))
* **create-message-form:** remove conditional rendering to allow css transitions ([6161823](https://gitlab.com/kozlovvski/secret-message/commit/6161823a9b0eb49a8736dc253e8cbc602130c738))
* **firebase:** fix firebase initializeApp bugs ([072b32e](https://gitlab.com/kozlovvski/secret-message/commit/072b32e07ff397ad381325fc155cb3acc07e0424))
* fix a broken import ([d94eea8](https://gitlab.com/kozlovvski/secret-message/commit/d94eea8cf9dfb3733584b7e713e597a337262fc3))
* **eslint:** fix conflicting eslint settings ([92c2da6](https://gitlab.com/kozlovvski/secret-message/commit/92c2da65398153455b012330f7ec4eb565dcab68))
* **firebase:** make `createMessage` cloud functions return a whole message object instead of just id ([d76f5d5](https://gitlab.com/kozlovvski/secret-message/commit/d76f5d5063947b23261c04780384ba9ea6e02b70))
* **functions:** get uid from context in createMessage function ([cb51de7](https://gitlab.com/kozlovvski/secret-message/commit/cb51de75356b873b1fdaede5c074389570da5c57))
* **redux:** change typesafe-actions setup into redux toolkit setup ([ac084ea](https://gitlab.com/kozlovvski/secret-message/commit/ac084ea10cbadd1104580ad57675c87040e65a01))
* **typings:** adjust secret-message typings ([ba0ac45](https://gitlab.com/kozlovvski/secret-message/commit/ba0ac45fdc2ef1208f9d6cd9cece26524da7420d))
* **util:** add viewport height update helper for mobile devices ([667a540](https://gitlab.com/kozlovvski/secret-message/commit/667a540a7599b439f152eaeb34cb986fc50c11e5))

## 0.1.0 (2020-11-17)
