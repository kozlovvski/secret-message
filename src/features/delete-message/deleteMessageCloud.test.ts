import { deleteMessageCloud } from "./deleteMessageCloud";
import { createMessageCloud } from "features/add-new-message/createMessageCloud";

jest.setTimeout(10000);

describe("deleteMessageCloud", () => {
  test("should return a function", () => {
    expect(typeof deleteMessageCloud).toBe("function");
  });

  it("should reject when id is undefined", async () => {
    await expect(deleteMessageCloud(undefined as any)).rejects.toThrow();
  });

  it("should reject when id is not a string", async () => {
    await expect(deleteMessageCloud(true as any)).rejects.toThrow();
  });

  it("should reject when there is no document with provided id", async () => {
    await expect(deleteMessageCloud("some-false-id")).rejects.toThrow();
  });

  it("should resolve with true if there is a document with provided id", async () => {
    const doc = await createMessageCloud("test-message");
    const res = await deleteMessageCloud(doc.id);
    await expect(res).toBe(true);
  });
});
