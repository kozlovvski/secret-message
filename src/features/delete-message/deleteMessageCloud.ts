import firebase from "firebase-instance";
import "firebase/functions";

export const deleteMessageCloud = (id: string): Promise<true> =>
  firebase
    .functions()
    .httpsCallable("deleteMessage")({ id })
    .then((res) => res.data);
