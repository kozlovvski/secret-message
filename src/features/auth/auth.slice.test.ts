import firebase from "firebase-instance";

import merge from "lodash/merge";
import defaultState from "store/default-state";
import { getActualActions, mockStore } from "test/testUtils";

import auth, {
  changeAuthScreen,
  hideAuthScreen,
  logInUser,
  logOutUser,
  showAuthScreen,
  signInAction,
  signInError,
  signInRequest,
  signInSuccess,
  signUpAction,
  signUpError,
  signUpRequest,
  signUpSuccess,
} from "./auth.slice";

jest.mock("firebase-instance", () => ({
  auth: jest.fn(),
}));

describe("auth reducer", () => {
  it("should have a correct initial state", () => {
    expect(auth(undefined, {} as any)).toEqual(defaultState.auth);
  });

  it("should handle showAuthScreen", () => {
    expect(auth(undefined, { type: showAuthScreen })).toEqual(
      merge({}, defaultState.auth, {
        showScreen: true,
      })
    );
  });

  it("should handle hideAuthScreen", () => {
    expect(auth(undefined, { type: hideAuthScreen })).toEqual(
      merge({}, defaultState.auth, {
        showScreen: false,
      })
    );
  });

  it("should handle changeAuthScreen", () => {
    expect(
      auth(undefined, { type: changeAuthScreen, payload: "signUp" })
    ).toEqual(
      merge({}, defaultState.auth, {
        authScreen: "signUp",
      })
    );
  });

  it("should handle logInUser", () => {
    expect(auth(undefined, { type: logInUser })).toEqual(
      merge({}, defaultState.auth, {
        isLoggedIn: true,
      })
    );
  });

  it("should handle logOutUser", () => {
    expect(auth(undefined, { type: logOutUser })).toEqual(
      merge({}, defaultState.auth, {
        isLoggedIn: false,
        authScreen: "signIn",
        signUp: {
          success: false,
        },
        signIn: {
          success: false,
        },
      })
    );
  });

  it("should handle signUpRequest", () => {
    expect(auth(undefined, { type: signUpRequest })).toEqual(
      merge({}, defaultState.auth, {
        signUp: {
          loading: true,
        },
      })
    );
  });

  it("should handle signUpSuccess", () => {
    expect(
      auth(undefined, {
        type: signUpSuccess,
      })
    ).toEqual(
      merge({}, defaultState.auth, {
        signUp: {
          success: true,
          loading: false,
        },
      })
    );
  });

  it("should handle signUpError", () => {
    expect(
      auth(undefined, {
        type: signUpError,
        payload: "some message",
      })
    ).toEqual(
      merge({}, defaultState.auth, {
        signUp: {
          success: false,
          loading: false,
          error: "some message",
        },
      })
    );
  });

  it("should handle signInRequest", () => {
    expect(auth(undefined, { type: signInRequest })).toEqual(
      merge({}, defaultState.auth, {
        signIn: {
          loading: true,
        },
      })
    );
  });

  it("should handle signInSuccess", () => {
    expect(
      auth(undefined, {
        type: signInSuccess,
      })
    ).toEqual(
      merge({}, defaultState.auth, {
        signIn: {
          success: true,
          loading: false,
        },
      })
    );
  });

  it("should handle signInError", () => {
    expect(
      auth(undefined, {
        type: signInError,
        payload: "some message",
      })
    ).toEqual(
      merge({}, defaultState.auth, {
        signIn: {
          success: false,
          loading: false,
          error: "some message",
        },
      })
    );
  });

  describe("signUpAction thunk", () => {
    let store: ReturnType<typeof mockStore>;
    const signUpMock = jest.fn();

    beforeEach(() => {
      (firebase.auth as jest.Mocked<any>).mockReturnValue({
        createUserWithEmailAndPassword: signUpMock,
      });
      store = mockStore(defaultState);
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it("should dispatch expected actions on successful request", async () => {
      signUpMock.mockResolvedValue({
        user: {
          updateProfile: jest.fn(),
        },
      });
      const expectedActions = [
        signUpRequest.type,
        signUpSuccess.type,
        hideAuthScreen.type,
      ];

      await store.dispatch(
        signUpAction({
          email: "john.doe@gmail.com",
          password: "12345678",
          displayName: "JohnDoe",
        })
      );
      expect(getActualActions(store)).toEqual(expectedActions);
    });

    it("should dispatch expected actions on rejected request", async () => {
      (firebase.auth()
        .createUserWithEmailAndPassword as jest.Mock).mockRejectedValue(
        "some error"
      );

      const expectedActions = [signUpRequest.type, signUpError.type];

      await store.dispatch(signUpAction({} as any));
      expect(getActualActions(store)).toEqual(expectedActions);
    });
  });

  describe("signInAction thunk", () => {
    let store: ReturnType<typeof mockStore>;
    const signInMock = jest.fn();

    beforeEach(() => {
      (firebase.auth as jest.Mocked<any>).mockReturnValue({
        signInWithEmailAndPassword: signInMock,
      });
      store = mockStore(defaultState);
    });

    afterEach(() => {
      jest.clearAllMocks();
    });

    it("should dispatch expected actions on successful request", async () => {
      signInMock.mockResolvedValue({
        user: {
          displayName: "JohnDoe",
        },
      });
      const expectedActions = [
        signInRequest.type,
        signInSuccess.type,
        hideAuthScreen.type,
      ];

      await store.dispatch(
        signInAction({
          email: "jd@hotmail.com",
          password: "test123pass",
        })
      );
      expect(getActualActions(store)).toEqual(expectedActions);
    });

    it("should dispatch expected actions on rejected request", async () => {
      (firebase.auth()
        .signInWithEmailAndPassword as jest.Mock).mockRejectedValue(
        "some error"
      );

      const expectedActions = [signInRequest.type, signInError.type];

      await store.dispatch(signInAction({} as any));
      expect(getActualActions(store)).toEqual(expectedActions);
    });
  });
});
