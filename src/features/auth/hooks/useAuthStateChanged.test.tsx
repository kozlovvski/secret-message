import useAuthStateChanged from "./useAuthStateChanged";
import firebase from "firebase-instance";
import { mockDispatch } from "react-redux";
import { logInUser, logOutUser } from "../auth.slice";
import { getUserMessages } from "features/user-panel/user-messages.slice";

jest.mock("firebase-instance", () => ({
  auth: jest.fn(),
}));

jest.mock("features/user-panel/user-messages.slice", () => ({
  getUserMessages: jest.fn(),
}));

describe("useAuthStateChanged", () => {
  beforeEach(() => {
    (firebase.auth as jest.Mocked<any>).mockReturnValue({
      onAuthStateChanged: jest.fn(),
    });
  });

  test("should return void", () => {
    expect(useAuthStateChanged()).toBe(undefined);
  });

  describe("if user is truthy", () => {
    beforeEach(() => {
      (firebase.auth as jest.Mocked<any>).mockReturnValue({
        onAuthStateChanged: (cb: (user: true) => void) => {
          cb(true);
        },
      });
    });

    test("should dispatch dispatch login action", () => {
      useAuthStateChanged();
      expect(mockDispatch).toBeCalledWith(logInUser());
    });

    test("should dispatch dispatch get user messages action", () => {
      useAuthStateChanged();
      expect(mockDispatch).toBeCalledWith(getUserMessages());
    });
  });

  describe("if user is falsy", () => {
    beforeEach(() => {
      (firebase.auth as jest.Mocked<any>).mockReturnValue({
        onAuthStateChanged: (cb: () => void) => {
          cb();
        },
      });
    });

    test("should dispatch logout action", () => {
      useAuthStateChanged();
      expect(mockDispatch).toBeCalledWith(logOutUser());
    });
  });
});
