import "firebase/firestore";

import { deleteMessageCloud } from "features/delete-message/deleteMessageCloud";
import defaultState from "store/default-state";
import { getActualActions, mockStore } from "test/testUtils";

import { getUserMessagesCloud } from "./getUserMessagesCloud";
import userMessages, {
  clearMessages,
  deleteMessage,
  getUserMessages,
  getUserMessagesError,
  getUserMessagesRequest,
  getUserMessagesSuccess,
  updateMessages,
} from "./user-messages.slice";

jest.mock("./getUserMessagesCloud", () => ({
  getUserMessagesCloud: jest.fn(),
}));

jest.mock("features/delete-message/deleteMessageCloud", () => ({
  deleteMessageCloud: jest.fn(),
}));

describe("user messages reducer", () => {
  it("should have a correct initial state", () => {
    expect(userMessages(undefined, {} as any)).toEqual({
      loading: false,
      success: false,
      messages: [],
    });
  });

  it("should handle getUserMessagesRequest", () => {
    expect(userMessages(undefined, { type: getUserMessagesRequest })).toEqual({
      loading: true,
      success: false,
      messages: [],
    });
  });

  it("should handle getUserMessagesSuccess", () => {
    expect(
      userMessages(undefined, {
        type: getUserMessagesSuccess,
        payload: [],
      })
    ).toEqual({
      loading: false,
      success: true,
      messages: [],
    });
  });

  it("should handle getUserMessagesError", () => {
    expect(
      userMessages(undefined, {
        type: getUserMessagesError,
        payload: "some message",
      })
    ).toEqual({
      loading: false,
      success: false,
      error: "some message",
      messages: [],
    });
  });

  it("should handle clearMessages", () => {
    expect(
      userMessages(undefined, {
        type: clearMessages,
      })
    ).toEqual({
      loading: false,
      success: false,
      messages: [],
    });
  });

  it("should handle updateMessages", () => {
    expect(
      userMessages(undefined, {
        type: updateMessages,
        payload: [
          {
            id: "some message",
          },
        ],
      })
    ).toEqual({
      loading: false,
      success: false,
      messages: [
        {
          id: "some message",
        },
      ],
    });
  });

  describe("getUserMessages thunk", () => {
    let store: ReturnType<typeof mockStore>;
    beforeEach(() => {
      store = mockStore(defaultState);
    });

    it("should dispatch expected actions on successful request", async () => {
      (getUserMessagesCloud as jest.Mock).mockResolvedValue([
        {
          id: "8123",
          createdAt: 9412833,
        },
      ]);

      const expectedActions = [
        getUserMessagesRequest.type,
        getUserMessagesSuccess.type,
      ];

      await store.dispatch(getUserMessages());
      expect(getActualActions(store)).toEqual(expectedActions);
    });

    it("should dispatch expected actions on rejected request", async () => {
      (getUserMessagesCloud as jest.Mock).mockRejectedValue("some error");

      const expectedActions = [
        getUserMessagesRequest.type,
        getUserMessagesError.type,
      ];

      await store.dispatch(getUserMessages());
      expect(getActualActions(store)).toEqual(expectedActions);
    });
  });

  describe("deleteMessage thunk", () => {
    let store: ReturnType<typeof mockStore>;
    beforeEach(() => {
      store = mockStore(defaultState);
    });

    it("should dispatch expected actions", async () => {
      (deleteMessageCloud as jest.Mock).mockResolvedValue(true);

      const expectedActions = [updateMessages.type];

      await store.dispatch(deleteMessage("5"));
      expect(getActualActions(store)).toEqual(expectedActions);
    });
  });
});
