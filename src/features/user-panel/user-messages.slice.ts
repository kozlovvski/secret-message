import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { deleteMessageCloud } from "features/delete-message/deleteMessageCloud";
import defaultState from "store/default-state";
import { GenericSMessage } from "typings/secret-message";
import { AppThunk } from "typings/store";

import { getUserMessagesCloud } from "./getUserMessagesCloud";

type UserMessagesState = {
  loading: boolean;
  success: boolean;
  error?: string;
  messages: GenericSMessage[];
};

const initialState: UserMessagesState = defaultState.userMessages;

const userMessagesSlice = createSlice({
  name: "user-messages",
  initialState,
  reducers: {
    getUserMessagesRequest(state) {
      state.loading = true;
    },
    getUserMessagesSuccess(state, action: PayloadAction<GenericSMessage[]>) {
      state.messages = action.payload;
      state.success = true;
      state.loading = false;
    },
    getUserMessagesError(state, action: PayloadAction<string>) {
      state.error = action.payload;
      state.success = false;
      state.loading = false;
    },
    updateMessages(state, action: PayloadAction<GenericSMessage[]>) {
      state.messages = action.payload;
    },
    clearMessages(state) {
      state.messages = [];
      state.success = false;
      state.loading = false;
    },
  },
});

export const {
  getUserMessagesRequest,
  getUserMessagesSuccess,
  getUserMessagesError,
  updateMessages,
  clearMessages,
} = userMessagesSlice.actions;

export const getUserMessages = (): AppThunk => async (dispatch) => {
  dispatch(getUserMessagesRequest());
  try {
    const userMessages = await getUserMessagesCloud();
    dispatch(getUserMessagesSuccess(userMessages));
  } catch (err) {
    dispatch(getUserMessagesError(err));
  }
};

export const deleteMessage = (id: string): AppThunk => async (
  dispatch,
  getState
) => {
  deleteMessageCloud(id);
  dispatch(
    updateMessages(getState().userMessages.messages.filter((m) => m.id !== id))
  );
};

export default userMessagesSlice.reducer;
