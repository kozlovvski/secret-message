import { createSlice } from "@reduxjs/toolkit";
import defaultState from "store/default-state";

type NavigationState = {
  show: boolean;
};

const initialState: NavigationState = defaultState.navigation;

const navigationSlice = createSlice({
  name: "navigation",
  initialState,
  reducers: {
    showNav(state) {
      state.show = true;
    },
    hideNav(state) {
      state.show = false;
    },
  },
});

export const { showNav, hideNav } = navigationSlice.actions;

export default navigationSlice.reducer;
