import { merge } from "lodash";
import defaultState from "store/default-state";

import navigation, { hideNav, showNav } from "./navigation.slice";

describe("navigation reducer", () => {
  it("should have a correct initial state", () => {
    expect(navigation(undefined, {} as any)).toEqual(defaultState.navigation);
  });

  it("should handle showNav", () => {
    expect(navigation(undefined, { type: showNav })).toEqual(
      merge({}, defaultState.navigation, {
        show: true,
      })
    );
  });

  it("should handle hideNav", () => {
    expect(navigation(undefined, { type: hideNav })).toEqual(
      merge({}, defaultState.navigation, {
        show: false,
      })
    );
  });
});
