import { getUserMessagesCloud } from "./getUserMessagesCloud";
import firebase from "firebase-instance";
import "firebase/auth";

describe("getUserMessagesCloud", () => {
  test("should return a function", () => {
    expect(typeof getUserMessagesCloud).toBe("function");
  });

  it("should reject when user is not logged in", async () => {
    await expect(getUserMessagesCloud()).rejects.toThrow();
  });

  it("should resolve when user is logged in", async () => {
    await firebase
      .auth()
      .signInWithEmailAndPassword("test@user.com", "12345678");
    await expect(getUserMessagesCloud()).resolves.toBeTruthy();
    await firebase.auth().signOut();
  }, 15000);

  it("should resolve with an array when user is logged in", async () => {
    await firebase
      .auth()
      .signInWithEmailAndPassword("test@user.com", "12345678");
    const data = await getUserMessagesCloud();
    expect(Array.isArray(data)).toBe(true);
    await firebase.auth().signOut();
  }, 15000);
});
