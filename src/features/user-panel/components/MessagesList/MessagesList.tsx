// templates/component/Component.tsx
import { DeleteOutlined, LinkOutlined } from "@ant-design/icons";
import {
  ConfigProvider,
  List,
  message,
  Popconfirm,
  Tooltip,
  Typography,
} from "antd";
import useAppSelector from "hooks/useAppSelector";
import React from "react";
import { GenericSMessage } from "typings/secret-message";
import copy from "copy-to-clipboard";

import styles from "./MessagesList.module.scss";
import useAppDispatch from "hooks/useAppDispatch";
import { deleteMessage } from "features/user-panel/user-messages.slice";

export interface IMessagesListProps {
  children?: never;
  alreadyViewed: boolean;
}

/**
 * A component that displays a list of user's messages
 *
 * @return the MessagesList component
 */

const MessagesList: React.FC<IMessagesListProps> = ({ alreadyViewed }) => {
  const { messages, loading } = useAppSelector((state) => state.userMessages);
  const dispatch = useAppDispatch();

  const copyHandler = (id: string) => {
    copy(`${window.location.host}/message/${id}`);
    message.success("Link copied to clipboard!");
  };

  const deleteHandler = (id: string) => {
    dispatch(deleteMessage(id));
    message.success("Done. Chosen message was deleted.");
  };

  return (
    <div
      className={styles["MessagesList"]}
      data-testid="component-MessagesList"
    >
      <ConfigProvider
        renderEmpty={() => (
          <div className={styles["empty"]} data-testid="empty">
            There are no messages of this type.
          </div>
        )}
      >
        <Typography.Title
          level={2}
          className={styles["list__title"]}
          data-testid="title"
        >
          {alreadyViewed ? "Already" : "Never"} viewed:
        </Typography.Title>
        <List<GenericSMessage>
          className={`${styles["list"]} ${
            alreadyViewed ? styles["viewed"] : ""
          }`}
          data-testid="list"
          size="small"
          dataSource={messages.filter((m) => m.alreadyViewed === alreadyViewed)}
          bordered
          loading={loading}
          renderItem={(m) => (
            <List.Item data-testid="list-item" className={styles["list__item"]}>
              <Tooltip
                arrowPointAtCenter
                placement="bottomLeft"
                title="Copy link to clipboard"
              >
                <LinkOutlined
                  className={styles["list__item-link"]}
                  data-testid="list-item__link"
                  onClick={() => {
                    copyHandler(m.id);
                  }}
                />
              </Tooltip>
              {m.id}
              <div>
                <span className={styles["list__item-time"]}>
                  created: {new Date(m.createdAt).toLocaleString()}
                </span>
                <Popconfirm
                  arrowPointAtCenter
                  placement="left"
                  data-testid="list-item__delete"
                  title="Are you use you want to delete this message?"
                  okButtonProps={{ type: "primary", danger: true }}
                  okText="Delete"
                  onConfirm={() => {
                    deleteHandler(m.id);
                  }}
                >
                  <DeleteOutlined />
                </Popconfirm>
              </div>
            </List.Item>
          )}
        />
      </ConfigProvider>
    </div>
  );
};

export default MessagesList;
