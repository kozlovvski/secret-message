import { mount, shallow } from "enzyme";
import React from "react";
import { mockDispatch, useSelector } from "react-redux";
import defaultState from "store/default-state";

import { findByTestAttr } from "test/testUtils";
import MessagesList, { IMessagesListProps } from "./MessagesList";
import merge from "lodash/merge";
import copy from "copy-to-clipboard";
import { message } from "antd";
import { deleteMessage } from "features/user-panel/user-messages.slice";

jest.mock("copy-to-clipboard", () => ({
  __esModule: true,
  default: jest.fn(),
}));

jest.mock("antd", () => ({
  ...jest.requireActual<Record<string, string>>("antd"),
  message: {
    success: jest.fn(),
  },
}));

jest.mock("features/user-panel/user-messages.slice", () => ({
  deleteMessage: jest.fn(),
}));

const defaultProps: IMessagesListProps = {
  alreadyViewed: false,
};

describe("<MessagesList />", () => {
  const setup = (props?: Record<string, unknown>) =>
    shallow(<MessagesList {...defaultProps} {...props} />);
  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    (useSelector as jest.Mock).mockReturnValue(defaultState.userMessages);
    wrapper = setup();
  });

  test("should mount", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "component-MessagesList");
    expect(component.length).toBe(1);
  });

  test("should render a title", () => {
    const title = findByTestAttr(wrapper, "title");
    expect(title.length).toBe(1);
  });

  test("should render a 'Never viewed' text in title", () => {
    const title = findByTestAttr(wrapper, "title");
    expect(title.render().text()).toContain("Never viewed");
  });

  test("should render a list", () => {
    const list = findByTestAttr(wrapper, "list");
    expect(list.length).toBe(1);
  });

  describe("if alreadyViewed is true", () => {
    beforeEach(() => {
      (useSelector as jest.Mock).mockReturnValue(defaultState.userMessages);
      wrapper = setup({ alreadyViewed: true });
    });

    test("should mount", () => {
      expect(wrapper.length).toBe(1);
    });

    test("should render without an error", () => {
      const component = findByTestAttr(wrapper, "component-MessagesList");
      expect(component.length).toBe(1);
    });

    test("should render a title", () => {
      const title = findByTestAttr(wrapper, "title");
      expect(title.length).toBe(1);
    });

    test("should render a 'Already viewed' text in title", () => {
      const title = findByTestAttr(wrapper, "title");
      expect(title.render().text()).toContain("Already viewed");
    });

    test("should render a list", () => {
      const list = findByTestAttr(wrapper, "list");
      expect(list.length).toBe(1);
    });
  });
});

describe("mounted <MessagesList />", () => {
  const setup = (props?: Record<string, unknown>) =>
    mount(<MessagesList {...defaultProps} {...props} />);
  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    (useSelector as jest.Mock).mockReturnValue(defaultState.userMessages);
    wrapper = setup();
  });

  describe("if messages length is greater than 0", () => {
    beforeEach(() => {
      (useSelector as jest.Mock).mockReturnValue(
        merge({}, defaultState.userMessages, {
          messages: [
            {
              id: "5",
              createdAt: 123012391,
              alreadyViewed: false,
            },
          ],
        })
      );
      wrapper = setup();
    });

    test("should render a list item", () => {
      const listItem = findByTestAttr(wrapper, "list-item").find("Item");
      expect(listItem.length).toBe(1);
    });

    test("should render a list item link", () => {
      const listItemLink = findByTestAttr(wrapper, "list-item__link").find(
        "AntdIcon"
      );
      expect(listItemLink.length).toBe(1);
    });

    test("should show a correct message when list item link is clicked", () => {
      const listItemLink = findByTestAttr(wrapper, "list-item__link").find(
        "AntdIcon"
      );
      listItemLink.simulate("click");
      expect(copy).toBeCalled();
      expect(message.success).toBeCalled();
    });

    test("should render a popconfirm delete", () => {
      const deletePopconfirm = findByTestAttr(
        wrapper,
        "list-item__delete"
      ).find("Tooltip");
      expect(deletePopconfirm.length).toBe(1);
      deletePopconfirm.simulate("click");

      const confirmButton = wrapper
        .find(".ant-popconfirm")
        .find(".ant-btn-primary");

      confirmButton.simulate("click");

      expect(mockDispatch).toBeCalledWith(deleteMessage("5"));
      expect(message.success).toBeCalled();
    });
  });
});
