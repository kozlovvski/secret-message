// templates/component/Component.tsx

import { MenuOutlined } from "@ant-design/icons";
import { showNav } from "features/user-panel/navigation.slice";
import useAppDispatch from "hooks/useAppDispatch";
import React from "react";
import styles from "./ShowMenuButton.module.scss";

export interface IShowMenuButtonProps {
  children?: never;
}

/**
 * A component that shows menu on click
 *
 * @return the ShowMenuButton component
 */

const ShowMenuButton: React.FC<IShowMenuButtonProps> = () => {
  const dispatch = useAppDispatch();
  const clickHandler = () => {
    dispatch(showNav());
  };

  return (
    <MenuOutlined
      onClick={clickHandler}
      className={styles["ShowMenuButton"]}
      data-testid="component-ShowMenuButton"
    />
  );
};

export default ShowMenuButton;
