import { shallow } from "enzyme";
import { showNav } from "features/user-panel/navigation.slice";
import React from "react";
import { mockDispatch, useSelector } from "react-redux";
import defaultState from "store/default-state";

import { findByTestAttr } from "test/testUtils";
import ShowMenuButton, { IShowMenuButtonProps } from "./ShowMenuButton";

const defaultProps: IShowMenuButtonProps = {};

describe("<ShowMenuButton />", () => {
  const setup = (props?: Record<string, unknown>) =>
    shallow(<ShowMenuButton {...defaultProps} {...props} />);
  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    (useSelector as jest.Mock).mockReturnValue(defaultState.auth);
    wrapper = setup();
  });

  test("should mount", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "component-ShowMenuButton");
    expect(component.length).toBe(1);
  });

  test("should dispatch a correct action on click", () => {
    const component = findByTestAttr(wrapper, "component-ShowMenuButton");
    component.simulate("click");
    expect(mockDispatch).toBeCalledWith(showNav());
  });
});
