import { shallow } from "enzyme";
import { hideNav } from "features/user-panel/navigation.slice";
import React from "react";
import { mockDispatch, useSelector } from "react-redux";
import defaultState from "store/default-state";

import { findByTestAttr } from "test/testUtils";
import Navigation, { INavigationProps } from "./Navigation";

const defaultProps: INavigationProps = {};

describe("<Navigation />", () => {
  const setup = (props?: Record<string, unknown>) =>
    shallow(<Navigation {...defaultProps} {...props} />);
  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    (useSelector as jest.Mock)
      .mockReturnValueOnce(defaultState.auth)
      .mockReturnValueOnce(defaultState.navigation);
    wrapper = setup();
  });

  test("should mount", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "component-Navigation");
    expect(component.length).toBe(1);
  });

  test("should render a spinner", () => {
    const spinner = findByTestAttr(wrapper, "spinner");
    expect(spinner.length).toBe(1);
  });

  test("should render a navigation container", () => {
    const navigationContainer = findByTestAttr(wrapper, "navigation-container");
    expect(navigationContainer.length).toBe(1);
  });

  test("should render a navigation nav", () => {
    const navigationNav = findByTestAttr(wrapper, "navigation-nav");
    expect(navigationNav.length).toBe(1);
  });

  test("should render a navigation welcome", () => {
    const navigationWelcome = findByTestAttr(wrapper, "navigation-welcome");
    expect(navigationWelcome.length).toBe(1);
  });

  test("should render a navigation list", () => {
    const navigationList = findByTestAttr(wrapper, "navigation-list");
    expect(navigationList.length).toBe(1);
  });

  test("should render a navigation auth button", () => {
    const navigationAuthButton = findByTestAttr(
      wrapper,
      "navigation-auth-button"
    );
    expect(navigationAuthButton.length).toBe(1);
  });

  test("should dispatch a correct action on auth button click", () => {
    const navigationAuthButton = findByTestAttr(
      wrapper,
      "navigation-auth-button"
    );
    navigationAuthButton.simulate("click");
    expect(mockDispatch).toBeCalledWith(hideNav());
  });

  test("should render a navigation backdrop", () => {
    const navigationBackdrop = findByTestAttr(wrapper, "navigation-backdrop");
    expect(navigationBackdrop.length).toBe(1);
  });

  test("should dispatch a correct action on backdrop click", () => {
    const navigationBackdrop = findByTestAttr(wrapper, "navigation-backdrop");
    navigationBackdrop.simulate("click");
    expect(mockDispatch).toBeCalledWith(hideNav());
  });

  describe("if loggedIn is false", () => {
    beforeEach(() => {
      (useSelector as jest.Mock)
        .mockReturnValueOnce({
          ...defaultState.auth,
          isLoggedIn: false,
        })
        .mockReturnValueOnce(defaultState.navigation);
      wrapper = setup();
    });

    test("should render sign in / sing up button", () => {
      const childMatch = findByTestAttr(wrapper, "sign-in-up");
      expect(childMatch.length).toBe(1);
    });
  });

  describe("if loggedIn is true", () => {
    beforeEach(() => {
      (useSelector as jest.Mock)
        .mockReturnValueOnce({
          ...defaultState.auth,
          isLoggedIn: true,
        })
        .mockReturnValueOnce(defaultState.navigation);
      wrapper = setup();
    });

    test("should render sign out button", () => {
      const childMatch = findByTestAttr(wrapper, "sign-out");
      expect(childMatch.length).toBe(1);
    });
  });
});
