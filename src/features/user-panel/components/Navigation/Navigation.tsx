// templates/component/Component.tsx
import { LoadingOutlined } from "@ant-design/icons";
import ShowAuthButton from "features/auth/components/ShowAuthButton/ShowAuthButton";
import SignOutButton from "features/auth/components/SignOutButton/SignOutButton";
import useUser from "features/auth/hooks/useUser";
import { hideNav } from "features/user-panel/navigation.slice";
import useAppDispatch from "hooks/useAppDispatch";
import useAppSelector from "hooks/useAppSelector";
import React, { useRef } from "react";
import { Link } from "react-router-dom";
import { CSSTransition } from "react-transition-group";
import cssTransitionClasses from "util/cssTransitionClasses";

import styles from "./Navigation.module.scss";

export interface INavigationProps {
  children?: never;
}

/**
 * A component that handles user navigation
 *
 * @return the Navigation component
 */

const Navigation: React.FC<INavigationProps> = () => {
  const { isLoggedIn } = useAppSelector((state) => state.auth);
  const { show } = useAppSelector((state) => state.navigation);
  const user = useUser();
  const nodeRef = useRef(null);
  const dispatch = useAppDispatch();

  const hideHandler = () => {
    dispatch(hideNav());
  };

  return (
    <CSSTransition
      in={show}
      timeout={500}
      classNames={cssTransitionClasses(styles, "navigation")}
      data-testid="component-Navigation"
      nodeRef={nodeRef}
    >
      <div
        ref={nodeRef}
        className={styles["navigation__container"]}
        data-testid="navigation-container"
      >
        <nav className={styles["navigation"]} data-testid="navigation-nav">
          <div
            className={styles["navigation__welcome"]}
            data-testid="navigation-welcome"
          >
            {isLoggedIn
              ? `Hello, ${user?.displayName}! 🎉`
              : "Hello, good to see you! 🎉"}
          </div>
          <ul
            className={styles["navigation__list"]}
            data-testid="navigation-list"
          >
            <li>
              <Link to="/new" onClick={hideHandler}>
                Create new message
              </Link>
            </li>
            {isLoggedIn && (
              <li>
                <Link to="/your-messages" onClick={hideHandler}>
                  Your messages
                </Link>
              </li>
            )}
          </ul>
          <div
            onClick={hideHandler}
            className={styles["navigation__auth-button"]}
            data-testid="navigation-auth-button"
          >
            {isLoggedIn === undefined ? (
              <LoadingOutlined data-testid="spinner" />
            ) : isLoggedIn ? (
              <SignOutButton data-testid="sign-out" />
            ) : (
              <ShowAuthButton data-testid="sign-in-up" />
            )}
          </div>
        </nav>
        <div
          className={styles["navigation__backdrop"]}
          onClick={hideHandler}
          data-testid="navigation-backdrop"
        ></div>
      </div>
    </CSSTransition>
  );
};

export default Navigation;
