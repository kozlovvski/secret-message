import "firebase/functions";

import firebase from "firebase-instance";
import { GenericSMessage } from "typings/secret-message";

export const getUserMessagesCloud = (): Promise<GenericSMessage[]> =>
  firebase
    .functions()
    .httpsCallable("getUserMessages")()
    .then((res) => res.data);
