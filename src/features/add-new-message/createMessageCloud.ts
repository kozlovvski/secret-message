import firebase from "firebase-instance";
import "firebase/functions";
import { GenericSMessage } from "typings/secret-message";

export const createMessageCloud = (message: string): Promise<GenericSMessage> =>
  firebase
    .functions()
    .httpsCallable("createMessage")({ message })
    .then((res) => res.data);
