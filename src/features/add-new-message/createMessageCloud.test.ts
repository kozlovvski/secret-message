import { deleteMessageCloud } from "features/delete-message/deleteMessageCloud";

import { createMessageCloud } from "./createMessageCloud";

describe("createMessageCloud", () => {
  test("should return a function", () => {
    expect(typeof createMessageCloud).toBe("function");
  });

  it("should reject when message is undefined", async () => {
    await expect(createMessageCloud(undefined as any)).rejects.toThrow();
  });

  it("should reject when message is not a string", async () => {
    await expect(createMessageCloud(true as any)).rejects.toThrow();
    await expect(createMessageCloud(3 as any)).rejects.toThrow();
  });

  it("should resolve when message is a string", async () => {
    const call = createMessageCloud("test-id");
    const message = await call;
    await deleteMessageCloud(message.id);
    await expect(call).resolves.toBeTruthy;
  });
});

jest.setTimeout(10000);
