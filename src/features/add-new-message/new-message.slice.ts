import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { getUserMessages } from "features/user-panel/user-messages.slice";
import defaultState from "store/default-state";
import { CreateSMessagePayload, GenericSMessage } from "typings/secret-message";
import { AppThunk } from "typings/store";

import { createMessageCloud } from "./createMessageCloud";

type NewMessageState = {
  loading: boolean;
  success: boolean;
  message?: GenericSMessage;
  error?: string;
};

const initialState: NewMessageState = defaultState.newMessage;

const newMessageSlice = createSlice({
  name: "new-message",
  initialState,
  reducers: {
    createMessageRequest(state) {
      state.loading = true;
    },
    createMessageSuccess(state, action: PayloadAction<GenericSMessage>) {
      state.message = action.payload;
      state.success = true;
      state.loading = false;
    },
    createMessageError(state, action: PayloadAction<string>) {
      state.error = action.payload;
      state.success = false;
      state.loading = false;
    },
    clearMessage(state) {
      state.message = undefined;
      state.success = false;
      state.loading = false;
    },
  },
});

export const {
  createMessageRequest,
  createMessageSuccess,
  createMessageError,
  clearMessage,
} = newMessageSlice.actions;

export const createMessage = ({
  message,
}: CreateSMessagePayload): AppThunk => async (dispatch) => {
  dispatch(createMessageRequest());
  try {
    const newMessage = await createMessageCloud(message);
    dispatch(createMessageSuccess(newMessage));
    dispatch(getUserMessages());
  } catch (err) {
    dispatch(createMessageError(err));
  }
};

export default newMessageSlice.reducer;
