import "firebase/firestore";

import {
  getUserMessagesRequest,
  getUserMessagesSuccess,
} from "features/user-panel/user-messages.slice";
import defaultState from "store/default-state";
import { getActualActions, mockStore } from "test/testUtils";
import { GenericSMessage } from "typings/secret-message";

import { createMessageCloud } from "./createMessageCloud";
import newMessage, {
  clearMessage,
  createMessage,
  createMessageError,
  createMessageRequest,
  createMessageSuccess,
} from "./new-message.slice";

jest.mock("./createMessageCloud", () => ({
  createMessageCloud: jest.fn(),
}));

jest.mock("features/user-panel/getUserMessagesCloud", () => ({
  getUserMessagesCloud: jest.fn(),
}));

describe("new message reducer", () => {
  it("should have a correct initial state", () => {
    expect(newMessage(undefined, {} as any)).toEqual({
      loading: false,
      success: false,
    });
  });

  it("should handle createMessageRequest", () => {
    expect(newMessage(undefined, { type: createMessageRequest })).toEqual({
      loading: true,
      success: false,
    });
  });

  it("should handle createMessageSuccess", () => {
    expect(
      newMessage(undefined, {
        type: createMessageSuccess,
        payload: {
          id: "1",
          createdAt: 12398123,
        } as GenericSMessage,
      })
    ).toEqual({
      loading: false,
      success: true,
      message: {
        id: "1",
        createdAt: 12398123,
      },
    });
  });

  it("should handle createMessageError", () => {
    expect(
      newMessage(undefined, {
        type: createMessageError,
        payload: "some message",
      })
    ).toEqual({
      loading: false,
      success: false,
      error: "some message",
    });
  });

  it("should handle clearMessage", () => {
    expect(
      newMessage(undefined, {
        type: clearMessage,
      })
    ).toEqual({
      loading: false,
      success: false,
      message: undefined,
    });
  });

  describe("createMessage thunk", () => {
    let store: ReturnType<typeof mockStore>;
    beforeEach(() => {
      store = mockStore(defaultState);
    });
    it("should dispatch expected actions on successful request", async () => {
      (createMessageCloud as jest.Mock).mockResolvedValue({
        id: "8123",
        createdAt: 21398218389,
      });

      const expectedActions = [
        createMessageRequest.type,
        createMessageSuccess.type,
        getUserMessagesRequest.type,
        getUserMessagesSuccess.type,
      ];

      await store.dispatch(createMessage({ message: "test-message" }));
      expect(getActualActions(store)).toEqual(expectedActions);
    });

    it("should dispatch expected actions on rejected request", async () => {
      (createMessageCloud as jest.Mock).mockRejectedValue("some error");

      const expectedActions = [
        createMessageRequest.type,
        createMessageError.type,
      ];

      await store.dispatch(createMessage({ message: false as any }));
      expect(getActualActions(store)).toEqual(expectedActions);
    });
  });
});
