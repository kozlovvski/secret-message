import firebase from "firebase-instance";
import "firebase/functions";
import { FullSMessage } from "typings/secret-message";

export const getMessageCloud = (id: string): Promise<FullSMessage> =>
  firebase
    .functions()
    .httpsCallable("getMessage")({ id })
    .then((res) => res.data);
