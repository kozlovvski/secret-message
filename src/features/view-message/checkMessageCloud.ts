import firebase from "firebase-instance";
import "firebase/functions";

export const checkMessageCloud = (id: string): Promise<boolean> =>
  firebase
    .functions()
    .httpsCallable("checkMessage")({ id })
    .then((res) => res.data);
