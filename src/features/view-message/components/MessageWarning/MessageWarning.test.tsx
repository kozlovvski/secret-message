import { shallow } from "enzyme";
import { getMessage } from "features/view-message/view-message.slice";
import React from "react";
import { mockDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { findByTestAttr } from "test/testUtils";

import MessageWarning, { IMessageWarningProps } from "./MessageWarning";

const defaultProps: IMessageWarningProps = {};

jest.mock("react-router-dom", () => ({
  useParams: jest.fn(),
}));

jest.mock("features/view-message/view-message.slice", () => ({
  getMessage: jest.fn(),
}));

describe("<MessageWarning />", () => {
  const setup = (props?: Record<string, unknown>) =>
    shallow(<MessageWarning {...defaultProps} {...props} />);
  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    (useParams as jest.Mock).mockReturnValue({ id: "test-page-id" });
    wrapper = setup();
  });

  test("should mount", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "component-MessageWarning");
    expect(component.length).toBe(1);
  });

  test("should render a title", () => {
    const title = findByTestAttr(wrapper, "title");
    expect(title.length).toBe(1);
  });

  test("should render a description", () => {
    const description = findByTestAttr(wrapper, "description");
    expect(description.length).toBe(1);
  });

  test("should render a view message button", () => {
    const viewMessageButton = findByTestAttr(wrapper, "view-message-button");
    expect(viewMessageButton.length).toBe(1);
  });

  test("view message button should dispatch a correct action on click", () => {
    const viewMessageButton = findByTestAttr(wrapper, "view-message-button");
    viewMessageButton.simulate("click");
    expect(mockDispatch).toBeCalledWith(getMessage({ id: "test-page-id" }));
  });
});
