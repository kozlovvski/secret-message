// templates/component/Component.tsx
import { Button, Typography } from "antd";
import { getMessage } from "features/view-message/view-message.slice";
import useAppDispatch from "hooks/useAppDispatch";
import React from "react";
import { useParams } from "react-router-dom";

import styles from "./MessageWarning.module.scss";

export interface IMessageWarningProps {
  children?: never;
}

/**
 * A component that displays a warning before showing a message
 *
 * @return the MessageWarning component
 */

const MessageWarning: React.FC<IMessageWarningProps> = () => {
  const { id } = useParams<{ id: string }>();
  const dispatch = useAppDispatch();

  const clickHandler = () => {
    dispatch(getMessage({ id }));
  };

  return (
    <div
      className={`${styles["MessageWarning"]} wrapper`}
      data-testid="component-MessageWarning"
    >
      <Typography.Title className="title" data-testid="title">
        Warning!
      </Typography.Title>
      <Typography.Paragraph className="subtitle" data-testid="description">
        You are about to display a message hidden behind this link.
        <br />
        This can be done only once - after that the message will be
        automatically deleted and you won&apos;t be able to view it again
      </Typography.Paragraph>
      <Button
        className="wrapper--narrow"
        type="primary"
        block
        data-testid="view-message-button"
        onClick={clickHandler}
      >
        Show the message
      </Button>
    </div>
  );
};

export default MessageWarning;
