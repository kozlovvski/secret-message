// templates/component/Component.tsx
import { PlusOutlined } from "@ant-design/icons";
import { Button, Typography } from "antd";
import React from "react";
import { Link } from "react-router-dom";

import styles from "./NoMessage.module.scss";

export interface INoMessageProps {
  children?: never;
}

/**
 * A component that displays when there is no message with provided id
 *
 * @return the NoMessage component
 */

const NoMessage: React.FC<INoMessageProps> = () => {
  return (
    <div
      className={`${styles["NoMessage"]} wrapper wrapper--narrow`}
      data-testid="component-NoMessage"
    >
      <Typography.Title className="title" data-testid="title">
        No secret lives here
      </Typography.Title>
      <Typography.Paragraph className="subtitle" data-testid="description">
        It can either be already viewed or it may have never existed in the
        first place!
      </Typography.Paragraph>
      <Link to="/new">
        <Button
          icon={<PlusOutlined />}
          type="primary"
          block
          data-testid="new-message-button"
        >
          Create a secret of your own
        </Button>
      </Link>
    </div>
  );
};

export default NoMessage;
