import { shallow } from "enzyme";
import React from "react";
import { findByTestAttr } from "test/testUtils";

import NoMessage, { INoMessageProps } from "./NoMessage";

const defaultProps: INoMessageProps = {};

describe("<NoMessage />", () => {
  const setup = (props?: Record<string, unknown>) =>
    shallow(<NoMessage {...defaultProps} {...props} />);
  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    wrapper = setup();
  });

  test("should mount", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "component-NoMessage");
    expect(component.length).toBe(1);
  });

  test("should render a title", () => {
    const title = findByTestAttr(wrapper, "title");
    expect(title.length).toBe(1);
  });

  test("should render a description", () => {
    const description = findByTestAttr(wrapper, "description");
    expect(description.length).toBe(1);
  });

  test("should render a create new message button", () => {
    const newMessageButton = findByTestAttr(wrapper, "new-message-button");
    expect(newMessageButton.length).toBe(1);
  });
});
