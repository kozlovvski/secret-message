import { shallow } from "enzyme";
import React from "react";
import { findByTestAttr } from "test/testUtils";

import LoadingMessage, { ILoadingMessageProps } from "./LoadingMessage";

const defaultProps: ILoadingMessageProps = {};

describe("<LoadingMessage />", () => {
  const setup = (props?: Record<string, unknown>) =>
    shallow(<LoadingMessage {...defaultProps} {...props} />);
  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    wrapper = setup();
  });

  test("should mount", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "component-LoadingMessage");
    expect(component.length).toBe(1);
  });

  test("should render a title and description", () => {
    const titleAndDesc = findByTestAttr(wrapper, "title-and-description");
    expect(titleAndDesc.length).toBe(1);
  });

  test("should render a create view message button", () => {
    const viewMessageButton = findByTestAttr(wrapper, "view-message-button");
    expect(viewMessageButton.length).toBe(1);
  });
});
