// templates/component/Component.tsx

import { Skeleton } from "antd";
import React from "react";
import styles from "./LoadingMessage.module.scss";

export interface ILoadingMessageProps {
  children?: never;
}

/**
 * A component that is displayed while app checks if a message with given `id` exists
 *
 * @return the LoadingMessage component
 */

const LoadingMessage: React.FC<ILoadingMessageProps> = () => {
  return (
    <div
      className={`${styles["LoadingMessage"]} wrapper`}
      data-testid="component-LoadingMessage"
    >
      <Skeleton
        className={styles["title"]}
        active
        title
        data-testid="title-and-description"
      />
      <Skeleton.Button
        className={styles["button"]}
        active
        data-testid="view-message-button"
      />
    </div>
  );
};

export default LoadingMessage;
