// templates/component/Component.tsx

import { PlusOutlined } from "@ant-design/icons";
import { Button, Input, Typography } from "antd";
import useAppSelector from "hooks/useAppSelector";
import React from "react";
import { Link } from "react-router-dom";
import styles from "./ViewMessage.module.scss";

export interface IViewMessageProps {
  children?: never;
}

/**
 * A component that <EDIT THIS>
 *
 * @return the ViewMessage component
 */

const ViewMessage: React.FC<IViewMessageProps> = () => {
  const {
    get: { message },
  } = useAppSelector((state) => state.viewMessage);
  return (
    <div
      className={`${styles["ViewMessage"]} wrapper`}
      data-testid="component-ViewMessage"
    >
      <Typography.Title className="title" data-testid="title">
        A secret message for you:
      </Typography.Title>
      <Input.TextArea
        className={styles["message-field"]}
        data-testid="message-field"
        value={message?.message}
        rows={5}
        disabled
      />
      <Typography.Paragraph className="footnote" data-testid="footnote">
        Warning: you won&apos;t be able to view this message again
      </Typography.Paragraph>
      <Link to="/new">
        <Button
          icon={<PlusOutlined />}
          type="primary"
          block
          className="wrapper--narrow"
          data-testid="new-message-button"
        >
          Create a secret of your own
        </Button>
      </Link>
    </div>
  );
};

export default ViewMessage;
