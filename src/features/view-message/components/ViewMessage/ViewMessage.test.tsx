import { shallow } from "enzyme";
import React from "react";
import { useSelector } from "react-redux";
import defaultState from "store/default-state";
import { findByTestAttr } from "test/testUtils";

import ViewMessage, { IViewMessageProps } from "./ViewMessage";

const defaultProps: IViewMessageProps = {};

describe("<ViewMessage />", () => {
  const setup = (props?: Record<string, unknown>) =>
    shallow(<ViewMessage {...defaultProps} {...props} />);
  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    (useSelector as jest.Mock).mockReturnValue(defaultState.viewMessage);
    wrapper = setup();
  });

  test("should mount", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "component-ViewMessage");
    expect(component.length).toBe(1);
  });

  test("should render a title", () => {
    const title = findByTestAttr(wrapper, "title");
    expect(title.length).toBe(1);
  });

  test("should render a message", () => {
    const description = findByTestAttr(wrapper, "message-field");
    expect(description.length).toBe(1);
  });

  test("should render a footnote", () => {
    const description = findByTestAttr(wrapper, "footnote");
    expect(description.length).toBe(1);
  });

  test("should render a create new message button", () => {
    const newMessageButton = findByTestAttr(wrapper, "new-message-button");
    expect(newMessageButton.length).toBe(1);
  });
});
