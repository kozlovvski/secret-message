import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import defaultState from "store/default-state";
import { CheckSMessagePayload, FullSMessage } from "typings/secret-message";
import { AppThunk } from "typings/store";
import { checkMessageCloud } from "./checkMessageCloud";
import { getMessageCloud } from "./getMessageCloud";

type ViewMessageState = {
  check: {
    loading: boolean;
    success: boolean;
    error?: string;
    exists?: boolean;
  };
  get: {
    loading: boolean;
    success: boolean;
    error?: string;
    message?: FullSMessage;
  };
};

const initialState: ViewMessageState = defaultState.viewMessage;

const viewMessageSlice = createSlice({
  name: "view-message",
  initialState,
  reducers: {
    checkMessageRequest(state) {
      state.check.loading = true;
    },
    checkMessageSuccess(state, action: PayloadAction<boolean>) {
      state.check.exists = action.payload;
      state.check.success = true;
      state.check.loading = false;
    },
    checkMessageError(state, action: PayloadAction<string>) {
      state.check.error = action.payload;
      state.check.success = false;
      state.check.loading = false;
    },
    getMessageRequest(state) {
      state.get.loading = true;
    },
    getMessageSuccess(state, action: PayloadAction<FullSMessage>) {
      state.get.message = action.payload;
      state.get.success = true;
      state.get.loading = false;
    },
    getMessageError(state, action: PayloadAction<string>) {
      state.get.error = action.payload;
      state.get.success = false;
      state.get.loading = false;
    },
    clearMessage(state) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      state = defaultState.viewMessage;
    },
  },
});

export const {
  checkMessageRequest,
  checkMessageSuccess,
  checkMessageError,
  getMessageRequest,
  getMessageSuccess,
  getMessageError,
  clearMessage,
} = viewMessageSlice.actions;

export const checkMessage = ({ id }: CheckSMessagePayload): AppThunk => async (
  dispatch
) => {
  dispatch(checkMessageRequest());
  try {
    const newMessage = await checkMessageCloud(id);
    dispatch(checkMessageSuccess(newMessage));
  } catch (err) {
    dispatch(checkMessageError(err));
  }
};

export const getMessage = ({ id }: CheckSMessagePayload): AppThunk => async (
  dispatch
) => {
  dispatch(getMessageRequest());
  try {
    const newMessage = await getMessageCloud(id);
    dispatch(getMessageSuccess(newMessage));
  } catch (err) {
    dispatch(getMessageError(err));
  }
};

export default viewMessageSlice.reducer;
