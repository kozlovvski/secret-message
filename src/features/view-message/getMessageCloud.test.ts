import { createMessageCloud } from "features/add-new-message/createMessageCloud";
import { checkMessageCloud } from "./checkMessageCloud";
import { getMessageCloud } from "./getMessageCloud";

describe("getMessageCloud", () => {
  test("should return a function", () => {
    expect(typeof getMessageCloud).toBe("function");
  });

  it("should reject when id is undefined", async () => {
    await expect(getMessageCloud(undefined as any)).rejects.toThrow();
  });

  it("should reject when id is not a string", async () => {
    await expect(getMessageCloud(false as any)).rejects.toThrow();
    await expect(getMessageCloud(8 as any)).rejects.toThrow();
  });

  it("should reject when id is not correct", async () => {
    await expect(getMessageCloud("test-id")).rejects.toThrow();
  });

  it("should resolve with an object", async () => {
    const { id } = await createMessageCloud("test-message");
    setTimeout(async () => {
      const message = await getMessageCloud(id);
      expect(typeof message).toBe("object");
    }, 1000);
  }, 10000);

  it("should delete the message", async () => {
    const { id } = await createMessageCloud("some-test-message");
    setTimeout(async () => {
      await getMessageCloud(id);
      setTimeout(async () => {
        expect(await checkMessageCloud(id)).toBe(false);
      }, 1000);
    }, 1000);
  }, 10000);
});
