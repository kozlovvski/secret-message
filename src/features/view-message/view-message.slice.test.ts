import "firebase/firestore";

import { merge } from "lodash";
import defaultState from "store/default-state";
import { getActualActions, mockStore } from "test/testUtils";

import { checkMessageCloud } from "./checkMessageCloud";
import { getMessageCloud } from "./getMessageCloud";
import viewMessage, {
  checkMessage,
  checkMessageError,
  checkMessageRequest,
  checkMessageSuccess,
  getMessage,
  getMessageError,
  getMessageRequest,
  getMessageSuccess,
} from "./view-message.slice";

jest.mock("./checkMessageCloud", () => ({
  checkMessageCloud: jest.fn(),
}));

jest.mock("./getMessageCloud", () => ({
  getMessageCloud: jest.fn(),
}));

describe("view message reducer", () => {
  it("should have a correct initial state", () => {
    expect(viewMessage(undefined, {} as any)).toEqual(defaultState.viewMessage);
  });

  it("should handle checkMessageRequest", () => {
    expect(viewMessage(undefined, { type: checkMessageRequest })).toEqual(
      merge({}, defaultState.viewMessage, {
        check: {
          loading: true,
        },
      })
    );
  });

  it("should handle checkMessageSuccess", () => {
    expect(
      viewMessage(undefined, { type: checkMessageSuccess, payload: false })
    ).toEqual(
      merge({}, defaultState.viewMessage, {
        check: {
          loading: false,
          exists: false,
          success: true,
        },
      })
    );
  });

  it("should handle checkMessageError", () => {
    expect(
      viewMessage(undefined, { type: checkMessageError, payload: "test error" })
    ).toEqual(
      merge({}, defaultState.viewMessage, {
        check: {
          loading: false,
          success: false,
          error: "test error",
        },
      })
    );
  });

  it("should handle getMessageRequest", () => {
    expect(viewMessage(undefined, { type: getMessageRequest })).toEqual(
      merge({}, defaultState.viewMessage, {
        get: {
          loading: true,
        },
      })
    );
  });

  it("should handle getMessageSuccess", () => {
    expect(
      viewMessage(undefined, {
        type: getMessageSuccess,
        payload: {
          id: "1",
          createdAt: 93129312,
          message: "test message",
        },
      })
    ).toEqual(
      merge({}, defaultState.viewMessage, {
        get: {
          loading: false,
          message: {
            id: "1",
            createdAt: 93129312,
            message: "test message",
          },
          success: true,
        },
      })
    );
  });

  it("should handle getMessageError", () => {
    expect(
      viewMessage(undefined, { type: getMessageError, payload: "test error" })
    ).toEqual(
      merge({}, defaultState.viewMessage, {
        get: {
          loading: false,
          success: false,
          error: "test error",
        },
      })
    );
  });

  describe("checkMessage thunk", () => {
    let store: ReturnType<typeof mockStore>;
    beforeEach(() => {
      store = mockStore(defaultState);
    });
    it("should dispatch expected actions on successful request", async () => {
      (checkMessageCloud as jest.Mock).mockResolvedValue(true);

      const expectedActions = [
        checkMessageRequest.type,
        checkMessageSuccess.type,
      ];

      await store.dispatch(checkMessage({ id: "test-message-id-3" }));
      expect(getActualActions(store)).toEqual(expectedActions);
    });

    it("should dispatch expected actions on rejected request", async () => {
      (checkMessageCloud as jest.Mock).mockRejectedValue("some error");

      const expectedActions = [
        checkMessageRequest.type,
        checkMessageError.type,
      ];

      await store.dispatch(checkMessage({ id: 3 as any }));
      expect(getActualActions(store)).toEqual(expectedActions);
    });
  });

  describe("getMessage thunk", () => {
    let store: ReturnType<typeof mockStore>;
    beforeEach(() => {
      store = mockStore(defaultState);
    });
    it("should dispatch expected actions on successful request", async () => {
      (getMessageCloud as jest.Mock).mockResolvedValue(true);

      const expectedActions = [getMessageRequest.type, getMessageSuccess.type];

      await store.dispatch(getMessage({ id: "test-message-id-3" }));
      expect(getActualActions(store)).toEqual(expectedActions);
    });

    it("should dispatch expected actions on rejected request", async () => {
      (getMessageCloud as jest.Mock).mockRejectedValue("some error");

      const expectedActions = [getMessageRequest.type, getMessageError.type];

      await store.dispatch(getMessage({ id: 3 as any }));
      expect(getActualActions(store)).toEqual(expectedActions);
    });
  });
});
