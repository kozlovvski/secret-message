import { checkMessageCloud } from "./checkMessageCloud";

describe("checkMessageCloud", () => {
  test("should return a function", () => {
    expect(typeof checkMessageCloud).toBe("function");
  });

  it("should reject when id is undefined", async () => {
    await expect(checkMessageCloud(undefined as any)).rejects.toThrow();
  });

  it("should reject when id is not a string", async () => {
    await expect(checkMessageCloud(true as any)).rejects.toThrow();
    await expect(checkMessageCloud(3 as any)).rejects.toThrow();
  });

  it("should resolve when id is a string", async () => {
    await expect(checkMessageCloud("test-id")).resolves.toBeTruthy;
  });

  it("should resolve with a boolean", async () => {
    await expect(typeof (await checkMessageCloud("test-id"))).toBe("boolean");
  });
});
