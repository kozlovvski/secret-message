export const adjustVH = () => {
  const vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);
};

export default () => {
  window.addEventListener("resize", adjustVH);
};
