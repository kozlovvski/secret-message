import fixVH, { adjustVH } from "./fixVH";

describe("fixVH", () => {
  test("should be a function", () => {
    expect(typeof fixVH).toBe("function");
  });

  test("should return void", () => {
    expect(fixVH()).toBe(undefined);
  });
});

describe("adjustVH", () => {
  test("should be a function", () => {
    expect(typeof adjustVH).toBe("function");
  });

  test("should return void", () => {
    expect(adjustVH()).toBe(undefined);
  });

  test("should set a property in document style", () => {
    const setProperty = jest.fn();
    Object.defineProperty(
      global.document.documentElement.style,
      "setProperty",
      { value: setProperty }
    );
    adjustVH();
    expect(setProperty).toHaveBeenCalled();
  });
});
