// templates/layout/Layout.tsx
import Logo from "components/Logo/Logo";
import React from "react";
import { Route, RouteProps, RouteChildrenProps } from "react-router-dom";

import styles from "./Default.module.scss";

/**
 * A layout component that wraps UI around a page. You probably want to use a `DefaultLayoutRoute` component inside a router and pass a page component into it
 *
 * @return the Default layout
 */

const DefaultLayout: React.FC = ({ children }) => {
  return (
    <div className={styles["Default"]} data-testid="layout-Default">
      <div className={styles["logo__container"]}>
        <Logo className={styles["logo"]} />
      </div>
      <div className={styles["content"]}>
        {children}
        <span className={styles["copy"]}>
          with ❤️ by <a href="https://gitlab.com/kozlovvski/">@kozlovvski</a>,
          &copy; 2020
        </span>
      </div>
    </div>
  );
};

interface IDefaultLayoutRouteProps extends RouteProps {
  component: React.ComponentType<RouteChildrenProps>;
}

/**
 * A custom react-router-dom `Route` component that wraps UI around a page.
 *
 * Usage:
 * ```
 * <Switch>
 *   {...}
 *   <DefaultLayoutRoute component={DefaultPage} {...otherProps} />
 * </Switch>
 *
 * ```
 *
 * @return the Default layout
 */

export const DefaultLayoutRoute: React.FC<IDefaultLayoutRouteProps> = ({
  component: Component,
  ...rest
}) => (
  <Route
    data-testid="layout-route-Default"
    {...rest}
    render={(matchProps) => (
      <DefaultLayout>
        <Component {...matchProps} />
      </DefaultLayout>
    )}
  />
);

export default DefaultLayout;
