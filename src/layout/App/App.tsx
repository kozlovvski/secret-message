// templates/layout/Layout.tsx
import Logo from "components/Logo/Logo";
import AuthScreen from "features/auth/components/AuthScreen/AuthScreen";
import Navigation from "features/user-panel/components/Navigation/Navigation";
import ShowMenuButton from "features/user-panel/components/ShowMenuButton/ShowMenuButton";
import useAppSelector from "hooks/useAppSelector";
import React, { useRef } from "react";
import { Link, Route, RouteChildrenProps, RouteProps } from "react-router-dom";
import { CSSTransition } from "react-transition-group";
import cssTransitionClasses from "util/cssTransitionClasses";

import styles from "./App.module.scss";

/**
 * A layout component that wraps UI around a page. You probably want to use a `AppLayoutRoute` component inside a router and pass a page component into it
 *
 * @return the App layout
 */

const AppLayout: React.FC = ({ children }) => {
  const { showScreen } = useAppSelector((state) => state.auth);
  const nodeRef = useRef(null);

  return (
    <div className={styles["App"]} data-testid="layout-App">
      <header className={styles["header"]}>
        <Link to="/" data-testid="logo">
          <Logo className={styles["logo"]} />
        </Link>
        <Navigation />
        <ShowMenuButton />
      </header>
      <main className={styles["content"]} data-testid="layout-content">
        {children}
      </main>
      <CSSTransition
        in={showScreen}
        timeout={500}
        classNames={cssTransitionClasses(styles, "auth-screen")}
        nodeRef={nodeRef}
      >
        <AuthScreen className={styles["auth-screen"]} forwardedRef={nodeRef} />
      </CSSTransition>
    </div>
  );
};

interface IAppLayoutRouteProps<P extends Record<string, string>>
  extends RouteProps {
  component: React.ComponentType<RouteChildrenProps<P>>;
}

/**
 * A custom react-router-dom `Route` component that wraps UI around a page.
 *
 * Usage:
 * ```
 * <Switch>
 *   {...}
 *   <AppLayoutRoute component={AppPage} {...otherProps} />
 * </Switch>
 *
 * ```
 *
 * @return the App layout
 */

export const AppLayoutRoute = <P extends Record<string, string>>({
  component: Component,
  ...rest
}: IAppLayoutRouteProps<P>) => (
  <Route<RouteProps>
    data-testid="layout-route-App"
    {...rest}
    render={(matchProps) => (
      <AppLayout>
        <Component {...matchProps} />
      </AppLayout>
    )}
  />
);

export default AppLayout;
