import "scss/global.scss";

import React from "react";
import ReactDOM from "react-dom";
import fixVH from "util/fixVH";

import App from "./app/App";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

fixVH();
