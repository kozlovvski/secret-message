import { useDispatch } from "react-redux";
import { AppDispatch } from "../typings/store";

/**
 * A correctly typed hook to access the redux `dispatch` function.
 *
 * @returns redux store's `dispatch` function
 *
 * @example
 *
 * import React from 'react'
 * import useAppDispatch from 'hooks/useAppDispatch'
 *
 * export const CounterComponent = ({ value }) => {
 *   const dispatch = useAppDispatch()
 *   return (
 *     <div>
 *       <span>{value}</span>
 *       <button onClick={() => dispatch({ type: 'increase-counter' })}>
 *         Increase counter
 *       </button>
 *     </div>
 *   )
 * }
 */
export default () => useDispatch<AppDispatch>();
