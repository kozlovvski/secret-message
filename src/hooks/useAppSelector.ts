import { useSelector } from "react-redux";
import { RootState } from "typings/store";

/**
 * A correctly typed hook to access the redux store's state. This hook takes a selector function
 * as an argument. The selector is called with the store state.
 *
 * @param selector the selector function
 *
 * @returns the selected state
 *
 * @example
 *
 * import React from 'react'
 * import useAppSelector from 'hooks/useAppSelector'
 *
 * export const CounterComponent = () => {
 *   const counter = useAppSelector((state) => state.counter)
 *   return <div>{counter}</div>
 * }
 */
export default <TState = RootState, TSelected = unknown>(
  selector: (state: TState) => TSelected
) => useSelector(selector);
