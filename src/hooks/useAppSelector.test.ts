import { useSelector } from "react-redux";

import useAppSelector from "./useAppSelector";

type DummyState = { testProp: "test value" };

describe("useAppSelector", () => {
  beforeEach(() => {
    (useSelector as jest.Mock).mockImplementation(
      (call: (state: DummyState) => unknown) => call({ testProp: "test value" })
    );
  });

  test("should return a call to useSelector function", () => {
    expect(useAppSelector((state: DummyState) => state.testProp)).toBe(
      useSelector((state: DummyState) => state.testProp)
    );
  });

  test("should return a correct value", () => {
    (useSelector as jest.Mock).mockReturnValue("test value");
    const returnValue = useAppSelector({} as any);
    expect(returnValue).toBe("test value");
  });
});
