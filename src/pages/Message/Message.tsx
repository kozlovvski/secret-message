// templates/page/Page.tsx
import LoadingMessage from "features/view-message/components/LoadingMessage/LoadingMessage";
import MessageWarning from "features/view-message/components/MessageWarning/MessageWarning";
import NoMessage from "features/view-message/components/NoMessage/NoMessage";
import ViewMessage from "features/view-message/components/ViewMessage/ViewMessage";
import {
  checkMessage,
  clearMessage,
} from "features/view-message/view-message.slice";
import useAppDispatch from "hooks/useAppDispatch";
import useAppSelector from "hooks/useAppSelector";
import React, { useEffect } from "react";
import { Helmet } from "react-helmet";
import { RouteChildrenProps } from "react-router-dom";

import styles from "./Message.module.scss";

interface IMessagePageProps extends RouteChildrenProps<{ id: string }> {
  children?: undefined;
}

/**
 * A page component which should be used inside react-router-dom `Route` component.
 * It doesn't accept children. If this component has many dependencies,
 * you probably want to use a lazy version of it. See `LazyMessagePage` in `./Message.lazy.tsx
 *
 * Usage:
 * ```
 * <Switch>
 *   {...}
 *   <Route component={MessagePage} {...otherProps} />
 * </Switch>
 *
 * ```
 *
 * @param props props inhereted from react-router-dom `Route` component
 *
 * @return the Message page component
 */

const MessagePage: React.FC<IMessagePageProps> = ({ match }) => {
  const id = match?.params?.id;
  const {
    check,
    check: { exists },
    get,
    get: { message },
  } = useAppSelector((state) => state.viewMessage);
  const dispatch = useAppDispatch();

  useEffect(() => {
    id && dispatch(checkMessage({ id }));
    () => {
      dispatch(clearMessage());
    };
  }, []);

  return (
    <div className={styles["Message"]} data-testid="page-Message">
      <Helmet>
        <title>View message | secret-message</title>
        <meta name="robots" content="noindex" />
      </Helmet>
      {/* if message is not yet checked, is during check request or during get request */}
      {exists === undefined || check.loading || get.loading ? (
        <LoadingMessage data-testid="loader" />
      ) : message ? (
        <ViewMessage data-testid="view-message" />
      ) : exists ? (
        <MessageWarning data-testid="message-warning" />
      ) : (
        <NoMessage data-testid="no-message" />
      )}
    </div>
  );
};

export default MessagePage;
