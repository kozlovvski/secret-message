// templates/page/Page.lazy.tsx

import React, { lazy, Suspense } from "react";
import { RouteChildrenProps } from "react-router-dom";

const MessageComponent = lazy(() => import("./Message"));

interface IMessagePageProps extends RouteChildrenProps<{ id: string }> {
  children?: never;
}

/**
 * A lazy version of Message page which should be used inside react-router-dom `Route` component.
 * It doesn't accept children.
 *
 * @example
 * ```
 * <Switch>
 *   {...}
 *   <Route component={LazyMessagePage} {...otherProps} />
 * </Switch>
 *
 * ```
 *
 * @param props props inhereted from react-router-dom `Route` component
 *
 * @return a lazy version of the Message page component
 */

const LazyMessagePage: React.FC<IMessagePageProps> = (props) => (
  <Suspense data-testid="suspense" fallback={null}>
    <MessageComponent {...props} />
  </Suspense>
);

export default LazyMessagePage;
