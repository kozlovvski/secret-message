import { mount, shallow } from "enzyme";
import { checkMessage } from "features/view-message/view-message.slice";
import merge from "lodash/merge";
import React from "react";
import { mockDispatch, useSelector } from "react-redux";
import defaultState from "store/default-state";
import { findByTestAttr, mockRouteChildrenProps } from "test/testUtils";

import MessagePage from "./Message";
import LazyMessagePage from "./Message.lazy";

jest.mock("features/view-message/checkMessageCloud", () => ({
  checkMessageCloud: jest.fn(),
}));

jest.mock("features/view-message/view-message.slice", () => ({
  __esModules: true,
  ...jest.requireActual<Record<string, unknown>>(
    "features/view-message/view-message.slice"
  ),
  checkMessage: jest.fn(),
}));

const testRouteProps = (id: string) =>
  mockRouteChildrenProps<{ id: string }>({
    params: { id },
    isExact: true,
    path: `message/${id}`,
    url: `localhost:3000/message/${id}`,
  });

describe("Message page", () => {
  const renderWrapper = (id = "test-id-1") =>
    shallow(<MessagePage {...testRouteProps(id)} />);
  let wrapper: ReturnType<typeof renderWrapper>;

  beforeEach(() => {
    (useSelector as jest.Mock).mockReturnValue(defaultState.viewMessage);
    wrapper = renderWrapper();
  });

  test("should mount when passed to a Route component", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "page-Message");
    expect(component.length).toBe(1);
  });

  test("should display a loader by default", () => {
    const loader = findByTestAttr(wrapper, "loader");
    expect(loader.length).toBe(1); // antd passes props down so the number of matches is higher
  });

  test("should call a checkMessageCloud function with id", () => {
    wrapper = mount(<MessagePage {...testRouteProps("test-id-1")} />) as any;
    expect(mockDispatch).toBeCalledWith(checkMessage({ id: "test-id-1" }));
  });

  describe("if message is already fetched", () => {
    beforeEach(() => {
      (useSelector as jest.Mock).mockReturnValue(
        merge({}, defaultState.viewMessage, {
          check: {
            exists: true,
            success: true,
          },
          get: {
            message: {
              id: "2312",
              createdAt: 54283321,
              message: "test message",
            },
            success: true,
          },
        })
      );
      wrapper = renderWrapper();
    });

    test("should render a ViewMessage component", () => {
      const viewMessage = findByTestAttr(wrapper, "view-message");
      expect(viewMessage.length).toBe(1);
    });
  });

  describe("if message exists", () => {
    beforeEach(() => {
      (useSelector as jest.Mock).mockReturnValue(
        merge({}, defaultState.viewMessage, {
          check: {
            exists: true,
            success: true,
          },
        })
      );
      wrapper = renderWrapper();
    });

    test("should render a MessageWarning component", () => {
      const messageWarning = findByTestAttr(wrapper, "message-warning");
      expect(messageWarning.length).toBe(1);
    });
  });

  describe("if message does not exist", () => {
    beforeEach(() => {
      (useSelector as jest.Mock).mockReturnValue(
        merge({}, defaultState.viewMessage, {
          check: {
            exists: false,
            success: true,
          },
        })
      );
      wrapper = renderWrapper();
    });

    test("should render a NoMessage component", () => {
      const viewMessage = findByTestAttr(wrapper, "no-message");
      expect(viewMessage.length).toBe(1);
    });
  });
});

describe("LazyMessage page", () => {
  const renderWrapper = (id = "some-id") =>
    shallow(<LazyMessagePage {...testRouteProps(id)} />);
  let wrapper: ReturnType<typeof renderWrapper>;

  beforeEach(() => {
    wrapper = renderWrapper();
  });

  test("should mount when passed to a Route component", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render a Suspense component", () => {
    const component = findByTestAttr(wrapper, "suspense");
    expect(component.length).toBe(1);
    expect(wrapper.find("Suspense").length).toBe(1);
  });
});
