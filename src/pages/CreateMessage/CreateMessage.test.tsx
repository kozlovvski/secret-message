import { mount, shallow } from "enzyme";
import { clearMessage } from "features/add-new-message/new-message.slice";
import React from "react";
import { mockDispatch, useSelector } from "react-redux";
import { MemoryRouter } from "react-router-dom";
import { findByTestAttr, mockRouteChildrenProps } from "test/testUtils";

import CreateMessagePage from "./CreateMessage";
import LazyCreateMessagePage from "./CreateMessage.lazy";

jest.mock("features/add-new-message/new-message.slice", () => ({
  clearMessage: jest.fn(),
}));

describe("CreateMessage page", () => {
  const renderWrapper = () =>
    shallow(<CreateMessagePage {...mockRouteChildrenProps({})} />);
  let wrapper: ReturnType<typeof renderWrapper>;

  beforeEach(() => {
    (useSelector as jest.Mock).mockReturnValue({
      success: false,
      loading: false,
    });
    wrapper = renderWrapper();
  });

  test("should mount when passed to a Route component", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "page-CreateMessage");
    expect(component.length).toBe(1);
  });

  describe("useEffect", () => {
    const renderWrapper = () =>
      mount(
        <MemoryRouter>
          <CreateMessagePage {...mockRouteChildrenProps({})} />
        </MemoryRouter>
      );
    let wrapper: ReturnType<typeof renderWrapper>;

    beforeEach(() => {
      (useSelector as jest.Mock).mockReturnValue({
        success: false,
        loading: false,
      });
      wrapper = renderWrapper();
    });

    test("should dispatch a correct action on cleanup", () => {
      wrapper.unmount();
      expect(mockDispatch).toHaveBeenCalledWith(clearMessage());
    });
  });
});

describe("LazyCreateMessage page", () => {
  const renderWrapper = () =>
    shallow(<LazyCreateMessagePage {...mockRouteChildrenProps({})} />);
  let wrapper: ReturnType<typeof renderWrapper>;

  beforeEach(() => {
    wrapper = renderWrapper();
  });

  test("should mount when passed to a Route component", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render a Suspense component", () => {
    const component = findByTestAttr(wrapper, "suspense");
    expect(component.length).toBe(1);
    expect(wrapper.find("Suspense").length).toBe(1);
  });
});
