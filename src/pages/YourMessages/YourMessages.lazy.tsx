// templates/page/Page.lazy.tsx

import React, { lazy, Suspense } from "react";
import { RouteChildrenProps } from "react-router-dom";

const YourMessagesComponent = lazy(() => import("./YourMessages"));

interface IYourMessagesPageProps extends RouteChildrenProps {
  children?: never;
}

/**
 * A lazy version of YourMessages page which should be used inside react-router-dom `Route` component.
 * It doesn't accept children.
 *
 * @example
 * ```
 * <Switch>
 *   {...}
 *   <Route component={LazyYourMessagesPage} {...otherProps} />
 * </Switch>
 *
 * ```
 *
 * @param props props inhereted from react-router-dom `Route` component
 *
 * @return a lazy version of the YourMessages page component
 */

const LazyYourMessagesPage: React.FC<IYourMessagesPageProps> = (props) => (
  <Suspense data-testid="suspense" fallback={null}>
    <YourMessagesComponent {...props} />
  </Suspense>
);

export default LazyYourMessagesPage;
