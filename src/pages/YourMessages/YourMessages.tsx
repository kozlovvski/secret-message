// templates/page/Page.tsx
import { Button, Skeleton, Typography } from "antd";
import View403 from "components/View403/View403";
import useUser from "features/auth/hooks/useUser";
import MessagesList from "features/user-panel/components/MessagesList/MessagesList";
import useAppSelector from "hooks/useAppSelector";
import React from "react";
import { Helmet } from "react-helmet";
import { Link, RouteChildrenProps } from "react-router-dom";

import styles from "./YourMessages.module.scss";

interface IYourMessagesPageProps extends RouteChildrenProps {
  children?: undefined;
}

/**
 * A page component which should be used inside react-router-dom `Route` component.
 * It doesn't accept children. If this component has many dependencies,
 * you probably want to use a lazy version of it. See `LazyYourMessagesPage` in `./YourMessages.lazy.tsx
 *
 * Usage:
 * ```
 * <Switch>
 *   {...}
 *   <Route component={YourMessagesPage} {...otherProps} />
 * </Switch>
 *
 * ```
 *
 * @param props props inhereted from react-router-dom `Route` component
 *
 * @return the YourMessages page component
 */

const YourMessagesPage: React.FC<IYourMessagesPageProps> = () => {
  const { messages, success } = useAppSelector((state) => state.userMessages);
  const user = useUser();

  return (
    <div className={styles["YourMessages"]} data-testid="page-YourMessages">
      <Helmet>
        <title>Your messages | secret-message</title>
        <meta name="robots" content="noindex" />
      </Helmet>
      {user ? (
        <div className="wrapper">
          <Typography.Title
            className={`${styles["title"]} title`}
            data-testid="title"
          >
            Your messages
          </Typography.Title>
          {success ? (
            messages.length === 0 ? (
              <>
                <Typography.Paragraph
                  className="subtitle"
                  data-testid="no-messages-description"
                >
                  Whoops. It looks like you haven&apos;t created any message. Do
                  it now!
                </Typography.Paragraph>
                <Link to="/new" data-testid="new-message-link">
                  <Button type="primary">Create a new secret-message</Button>
                </Link>
              </>
            ) : (
              <>
                <MessagesList
                  data-testid="messages-list"
                  alreadyViewed={false}
                />
                <MessagesList data-testid="messages-list" alreadyViewed />
              </>
            )
          ) : (
            <div className={styles["skeleton"]} data-testid="loader">
              <Skeleton active />
              <Skeleton active />
            </div>
          )}
        </div>
      ) : (
        <View403 data-testid="403-view" />
      )}
    </div>
  );
};

export default YourMessagesPage;
