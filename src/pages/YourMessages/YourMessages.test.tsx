import { shallow } from "enzyme";
import useUser from "features/auth/hooks/useUser";
import merge from "lodash/merge";
import React from "react";
import { useSelector } from "react-redux";
import defaultState from "store/default-state";
import { findByTestAttr, mockRouteChildrenProps } from "test/testUtils";

import YourMessagesPage from "./YourMessages";
import LazyYourMessagesPage from "./YourMessages.lazy";

jest.mock("features/auth/hooks/useUser", () => ({
  __esModule: true,
  default: jest.fn(),
}));

describe("YourMessages page", () => {
  const renderWrapper = () =>
    shallow(<YourMessagesPage {...mockRouteChildrenProps({})} />);
  let wrapper: ReturnType<typeof renderWrapper>;

  beforeEach(() => {
    (useSelector as jest.Mock).mockReturnValue(defaultState.userMessages);
    (useUser as jest.Mock).mockReturnValue({ displayName: "user" });
    wrapper = renderWrapper();
  });

  test("should mount when passed to a Route component", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "page-YourMessages");
    expect(component.length).toBe(1);
  });

  test("should render a loader", () => {
    const loader = findByTestAttr(wrapper, "loader");
    expect(loader.length).toBe(1);
  });

  describe("if success is true", () => {
    beforeEach(() => {
      (useSelector as jest.Mock).mockReturnValue(
        merge({}, defaultState.userMessages, { success: true })
      );
      wrapper = renderWrapper();
    });

    test("should render an appropriate description", () => {
      const description = findByTestAttr(wrapper, "no-messages-description");
      expect(description.length).toBe(1);
    });

    test("should render a new message link", () => {
      const newMessageLink = findByTestAttr(wrapper, "new-message-link");
      expect(newMessageLink.length).toBe(1);
    });

    describe("and if messages length is not 0", () => {
      beforeEach(() => {
        (useSelector as jest.Mock).mockReturnValue(
          merge({}, defaultState.userMessages, {
            success: true,
            messages: [
              {
                id: "129129",
                createdAt: 2931293,
                alreadyViewed: false,
              },
            ],
          })
        );
        wrapper = renderWrapper();
      });

      test("should render two messages lists", () => {
        const messagesLists = findByTestAttr(wrapper, "messages-list");
        expect(messagesLists.length).toBe(2);
      });
    });
  });

  describe("if user in falsy", () => {
    beforeEach(() => {
      (useSelector as jest.Mock).mockReturnValue(defaultState.userMessages);
      (useUser as jest.Mock).mockReturnValue(null);
      wrapper = renderWrapper();
    });

    test("should render a 403 view", () => {
      const view403 = findByTestAttr(wrapper, "403-view");
      expect(view403.length).toBe(1);
    });
  });
});

describe("LazyYourMessages page", () => {
  const renderWrapper = () =>
    shallow(<LazyYourMessagesPage {...mockRouteChildrenProps({})} />);
  let wrapper: ReturnType<typeof renderWrapper>;

  beforeEach(() => {
    wrapper = renderWrapper();
  });

  test("should mount when passed to a Route component", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render a Suspense component", () => {
    const component = findByTestAttr(wrapper, "suspense");
    expect(component.length).toBe(1);
    expect(wrapper.find("Suspense").length).toBe(1);
  });
});
