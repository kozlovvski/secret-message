// templates/page/Page.tsx
import { PlusOutlined } from "@ant-design/icons";
import { Button, Typography } from "antd";
import React from "react";
import { Helmet } from "react-helmet";
import { Link, RouteChildrenProps } from "react-router-dom";

import styles from "./Home.module.scss";

interface IHomePageProps extends RouteChildrenProps {
  children?: undefined;
}

/**
 * A page component which should be used inside react-router-dom `Route` component.
 * It doesn't accept children. If this component has many dependencies,
 * you probably want to use a lazy version of it. See `LazyHomePage` in `./Home.lazy.tsx
 *
 * Usage:
 * ```
 * <Switch>
 *   {...}
 *   <Route component={HomePage} {...otherProps} />
 * </Switch>
 *
 * ```
 *
 * @param props props inhereted from react-router-dom `Route` component
 *
 * @return the Home page component
 */

const HomePage: React.FC<IHomePageProps> = () => {
  return (
    <div className={styles["Home"]} data-testid="page-Home">
      <Helmet>
        <title>Home | secret-message</title>
        <meta
          name="description"
          content="When you send people passwords and private links via email or chat,
      there are copies of that information stored in many places. Do it securely using our service."
        />
      </Helmet>
      <Typography.Title className="title title--secondary title--no-underline">
        Easy way to share secrets 🎉
      </Typography.Title>
      <Typography.Paragraph className="subtitle subtitle--secondary">
        When you send people passwords and private links via email or chat,
        there are copies of that information stored in many places. Ever
        wondered how to do that securely? Simply use a one-time link instead,
        which will make sure that your private data is read only once - by the
        time your secret is displayed, it is already purged from our server,
        never to be accessed again.
      </Typography.Paragraph>
      <Link to="/new">
        <Button type="primary" icon={<PlusOutlined />}>
          Create your own secret
        </Button>
      </Link>
    </div>
  );
};

export default HomePage;
