// templates/page/Page.lazy.tsx

import React, { lazy, Suspense } from "react";
import { RouteChildrenProps } from "react-router-dom";

const Page404Component = lazy(() => import("./Page404"));

interface IPage404PageProps extends RouteChildrenProps {
  children?: never;
}

/**
 * A lazy version of Page404 page which should be used inside react-router-dom `Route` component.
 * It doesn't accept children.
 *
 * @example
 * ```
 * <Switch>
 *   {...}
 *   <Route component={LazyPage404Page} {...otherProps} />
 * </Switch>
 *
 * ```
 *
 * @param props props inhereted from react-router-dom `Route` component
 *
 * @return a lazy version of the Page404 page component
 */

const LazyPage404Page: React.FC<IPage404PageProps> = (props) => (
  <Suspense data-testid="suspense" fallback={null}>
    <Page404Component {...props} />
  </Suspense>
);

export default LazyPage404Page;
