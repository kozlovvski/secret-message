import { shallow } from "enzyme";
import React from "react";
import { findByTestAttr, mockRouteChildrenProps } from "test/testUtils";

import Page404Page from "./Page404";
import LazyPage404Page from "./Page404.lazy";

describe("Page404 page", () => {
  const renderWrapper = () =>
    shallow(<Page404Page {...mockRouteChildrenProps({})} />);
  let wrapper: ReturnType<typeof renderWrapper>;

  beforeEach(() => {
    wrapper = renderWrapper();
  });

  test("should mount when passed to a Route component", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "page-Page404");
    expect(component.length).toBe(1);
  });

  test("should render a title", () => {
    const title = findByTestAttr(wrapper, "404-title");
    expect(title.length).toBe(1);
  });

  test("should render a subtitle", () => {
    const subtitle = findByTestAttr(wrapper, "404-subtitle");
    expect(subtitle.length).toBe(1);
  });
});

describe("LazyPage404 page", () => {
  const renderWrapper = () =>
    shallow(<LazyPage404Page {...mockRouteChildrenProps({})} />);
  let wrapper: ReturnType<typeof renderWrapper>;

  beforeEach(() => {
    wrapper = renderWrapper();
  });

  test("should mount when passed to a Route component", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render a Suspense component", () => {
    const component = findByTestAttr(wrapper, "suspense");
    expect(component.length).toBe(1);
    expect(wrapper.find("Suspense").length).toBe(1);
  });
});
