// templates/page/Page.tsx

import { Button, Typography } from "antd";
import React from "react";
import { Link, RouteChildrenProps } from "react-router-dom";

import styles from "./Page404.module.scss";

interface IPage404PageProps extends RouteChildrenProps {
  children?: undefined;
}

/**
 * A page component which should be used inside react-router-dom `Route` component.
 * It doesn't accept children. If this component has many dependencies,
 * you probably want to use a lazy version of it. See `LazyPage404Page` in `./Page404.lazy.tsx
 *
 * Usage:
 * ```
 * <Switch>
 *   {...}
 *   <Route component={Page404Page} {...otherProps} />
 * </Switch>
 *
 * ```
 *
 * @param props props inhereted from react-router-dom `Route` component
 *
 * @return the Page404 page component
 */

const Page404Page: React.FC<IPage404PageProps> = () => {
  return (
    <div className={styles["Page404"]} data-testid="page-Page404">
      <Typography.Title className="title" data-testid="404-title">
        Whoops!
      </Typography.Title>
      <Typography.Paragraph className="subtitle" data-testid="404-subtitle">
        It looks like you are looking for a page that doesn&apos;t exist.
      </Typography.Paragraph>
      <Link to="/new">
        <Button type="primary">Go back to new message page</Button>
      </Link>
    </div>
  );
};

export default Page404Page;
