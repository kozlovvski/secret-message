import firebase from "firebase/app";
import "firebase/analytics";
import firebaseConfig from "./secret-message-eb337-firebase-config.json";

firebase.initializeApp(firebaseConfig);
firebase.analytics.isSupported().then((isSupported) => {
  isSupported && firebase.analytics();
});

export default firebase;
