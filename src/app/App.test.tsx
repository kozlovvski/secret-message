import { shallow } from "enzyme";
import React from "react";
import { findByTestAttr } from "test/testUtils";

import { App, WrappedApp } from "./App";

describe("<App />", () => {
  const setup = (props?: Record<string, unknown>) =>
    shallow(<App {...props} />);

  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    wrapper = setup();
  });

  test("should mount", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "component-App");
    expect(component.length).toBe(1);
  });

  test("should render a Routes compoennt", () => {
    const routes = wrapper.find("Routes");
    expect(routes).toBeTruthy();
  });
});

describe("<WrappedApp />", () => {
  const setup = (props?: Record<string, unknown>) =>
    shallow(<WrappedApp {...props} />);

  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    wrapper = setup();
  });

  test("should mount", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "component-WrappedApp");
    expect(component.length).toBe(1);
  });

  test("should render a Provider component", () => {
    const provider = wrapper.find("Provider");
    expect(provider).toBeTruthy();
  });

  test("should render a BrowserRouter component", () => {
    const browserRouter = wrapper.find("BrowserRouter");
    expect(browserRouter).toBeTruthy();
  });

  test("should render an App component", () => {
    const appComp = wrapper.find("App");
    expect(appComp).toBeTruthy();
  });
});
