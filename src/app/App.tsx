import React from "react";
import { BrowserRouter } from "react-router-dom";

import Routes from "routes/routes";
import { Provider } from "react-redux";
import store from "store";
import useAuthStateChanged from "features/auth/hooks/useAuthStateChanged";

export const App: React.FC = () => {
  useAuthStateChanged();

  return <Routes data-testid="component-App" />;
};

export const WrappedApp: React.FC = () => (
  <Provider store={store} data-testid="component-WrappedApp">
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

export default WrappedApp;
