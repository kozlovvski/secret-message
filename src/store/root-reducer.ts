import { combineReducers } from "redux";
import newMessage from "features/add-new-message/new-message.slice";
import auth from "features/auth/auth.slice";
import viewMessage from "features/view-message/view-message.slice";
import navigation from "features/user-panel/navigation.slice";
import userMessages from "features/user-panel/user-messages.slice";

export default combineReducers({
  newMessage,
  viewMessage,
  auth,
  navigation,
  userMessages,
});
