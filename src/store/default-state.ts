import { RootState } from "typings/store";

const defaultState: RootState = {
  newMessage: {
    success: false,
    loading: false,
  },
  userMessages: {
    success: false,
    loading: false,
    messages: [],
  },
  viewMessage: {
    check: {
      success: false,
      loading: false,
    },
    get: {
      success: false,
      loading: false,
    },
  },
  auth: {
    showScreen: false,
    authScreen: "signIn",
    signUp: {
      loading: false,
      success: false,
    },
    signIn: {
      loading: false,
      success: false,
    },
  },
  navigation: {
    show: false,
  },
};

export default defaultState;
