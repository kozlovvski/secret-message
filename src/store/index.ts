import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";

import rootReducer from "./root-reducer";

export const middleware = [
  ...getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: ["auth/logInUser"],
      ignoredPaths: ["firebase", "firestore"],
    },
  }),
];

export const store = configureStore({
  reducer: rootReducer,
  middleware,
});

export const hotAcceptCallback = (targetStore = store) => {
  const newRootReducer = require("./root-reducer").default;
  targetStore.replaceReducer(newRootReducer);
};

export const hotReloadHandler = (env = process.env.NODE_ENV, mod = module) => {
  if (env === "development" && mod.hot) {
    mod.hot.accept("./root-reducer.ts", hotAcceptCallback);
  }
};

hotReloadHandler();

export default store;
