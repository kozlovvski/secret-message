import { hotAcceptCallback, hotReloadHandler } from ".";

describe("hotReloadHandler", () => {
  test("should be a function", () => {
    expect(typeof hotReloadHandler).toBe("function");
  });

  test("should only call hot.accept() if env is development and hot is defined", () => {
    const accept = jest.fn();
    const mockModule = ({ hot: { accept } } as unknown) as NodeModule;
    hotReloadHandler("development", mockModule);
    expect(accept).toHaveBeenCalled();
  });

  test("should call hot.accept() with a correct callback", () => {
    const accept = jest.fn();
    const mockModule = { hot: { accept } } as any;
    hotReloadHandler("development", mockModule);
  });
});

describe("hotAcceptCallback", () => {
  test("should be a function", () => {
    expect(typeof hotAcceptCallback).toBe("function");
  });

  test("should return void", () => {
    expect(typeof hotAcceptCallback()).toBe("undefined");
  });

  test("should only call hot.accept() if env is development and hot is defined", () => {
    const replaceReducer = jest.fn();
    hotAcceptCallback({ replaceReducer } as any);
    expect(replaceReducer).toHaveBeenCalled();
  });
});
