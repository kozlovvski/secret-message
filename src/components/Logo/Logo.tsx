// templates/component/Component.tsx

import React from "react";
import styles from "./Logo.module.scss";

export interface ILogoProps {
  children?: never;
  className?: string;
}

/**
 * A component that <EDIT THIS>
 *
 * @return the Logo component
 */

const Logo: React.FC<ILogoProps> = ({ className = "" }) => {
  return (
    <div
      className={`${styles["Logo"]} ${className}`}
      data-testid="component-Logo"
    >
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 224 224">
        <defs />
        <defs />
        <path d="M136 179c0-6 2-13 8-18v-5a42 42 0 0141-37h3a42 42 0 0116 5l2-11 10-58 7-46a8 8 0 00-5-9 8 8 0 00-7 1l-39 23L4 121c-3 1-4 4-4 7s2 6 5 7l48 20a2 2 0 002-1L185 42l1-1h1l-2 2L81 171a4 4 0 00-1 2v42a8 8 0 005 8 8 8 0 009-3l29-35a2 2 0 013-1l10 5z" />
        <path d="M221 177c0-3-2-6-5-7l-3-1v-9c0-15-11-26-26-27h-2a27 27 0 00-27 25v12c-6 1-8 3-8 9v36c0 7 3 9 10 9h53c5 0 7-3 7-7l1-40zm-20-7h-30v-13c1-6 8-11 15-11h1a15 15 0 0114 14v10z" />
      </svg>
      secret-message
    </div>
  );
};

export default Logo;
