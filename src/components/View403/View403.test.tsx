import { shallow } from "enzyme";
import React from "react";
import { useSelector } from "react-redux";
import defaultState from "store/default-state";

import { findByTestAttr } from "test/testUtils";
import View403, { IView403Props } from "./View403";

const defaultProps: IView403Props = {};

describe("<View403 />", () => {
  const setup = (props?: Record<string, unknown>) =>
    shallow(<View403 {...defaultProps} {...props} />);
  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    (useSelector as jest.Mock).mockReturnValue(defaultState.auth);
    wrapper = setup();
  });

  test("should mount", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "component-View403");
    expect(component.length).toBe(1);
  });

  test("should render a title", () => {
    const title = findByTestAttr(wrapper, "403-title");
    expect(title.length).toBe(1);
  });

  test("should render a subtitle", () => {
    const subtitle = findByTestAttr(wrapper, "403-subtitle");
    expect(subtitle.length).toBe(1);
  });
});
