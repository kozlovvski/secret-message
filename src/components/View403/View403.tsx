// templates/component/Component.tsx

import { Typography } from "antd";
import ShowAuthButton from "features/auth/components/ShowAuthButton/ShowAuthButton";
import React from "react";
import styles from "./View403.module.scss";

export interface IView403Props {
  children?: never;
}

/**
 * A component that displays when access is forbidden
 *
 * @return the View403 component
 */

const View403: React.FC<IView403Props> = () => {
  return (
    <div className={styles["View403"]} data-testid="component-View403">
      <Typography.Title className="title" data-testid="403-title">
        Whoops!
      </Typography.Title>
      <Typography.Paragraph className="subtitle" data-testid="403-subtitle">
        It looks like you aren&apos;t allowed to view this page.
        <br />
        Did you forget to sign in?
      </Typography.Paragraph>
      <ShowAuthButton />
    </div>
  );
};

export default View403;
