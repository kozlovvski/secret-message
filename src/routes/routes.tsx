import MessagePage from "pages/Message/Message";
import LazyPage404 from "pages/Page404/Page404.lazy";
import YourMessagesPage from "pages/YourMessages/YourMessages";
import React from "react";
import { Route, Switch } from "react-router-dom";

import AppLayout from "../layout/App/App";
import { DefaultLayoutRoute } from "../layout/Default/Default";
import CreateMessagePage from "../pages/CreateMessage/CreateMessage";
import HomePage from "../pages/Home/Home";

const Routes: React.FC = () => (
  <Switch data-testid="component-Routes">
    <DefaultLayoutRoute exact path="/" component={HomePage} />
    <AppLayout>
      <Switch>
        <Route exact path="/new" component={CreateMessagePage} />
        <Route exact path="/your-messages" component={YourMessagesPage} />
        <Route exact path="/message/:id" component={MessagePage} />
        <Route component={LazyPage404} />
      </Switch>
    </AppLayout>
  </Switch>
);

export default Routes;
