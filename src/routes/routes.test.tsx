import { shallow } from "enzyme";
import React from "react";
import { findByTestAttr } from "test/testUtils";

import Routes from "./routes";

describe("routes", () => {
  const setup = (props?: Record<string, unknown>) =>
    shallow(<Routes {...props} />);

  let wrapper: ReturnType<typeof setup>;

  beforeEach(() => {
    wrapper = setup();
  });

  test("should mount", () => {
    expect(wrapper.length).toBe(1);
  });

  test("should render without an error", () => {
    const component = findByTestAttr(wrapper, "component-Routes");
    expect(component.length).toBe(1);
  });

  test("should render a Switch", () => {
    const switchComp = wrapper.find("Switch");
    expect(switchComp).toBeTruthy();
  });
});
